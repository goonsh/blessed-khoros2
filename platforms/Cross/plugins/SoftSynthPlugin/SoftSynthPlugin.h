/*
	SoftSynthPlugin.h
	SoftSynthPlugin primitives

	Created by NISHIHARA Satoshi on 13/07/29.
	Copyright 2013 goonsh@gmail.com. All rights reserved.
 */

/********************************************************************************/
/********************************************************************************/
/*							originals											*/
/********************************************************************************/
/********************************************************************************/
/* module initialization/shutdown */
int midiInit(void);
int midiShutdown(void);

int sqMIDIGetClock(void);
int sqMIDIGetPortCount(void);
int sqMIDIGetPortDirectionality(int portNum);
int sqMIDIGetPortName(int portNum, int namePtr, int length);
int sqMIDIClosePort(int portNum);
int sqMIDIOpenPort(int portNum, int readSemaIndex, int interfaceClockRate);
int sqMIDIParameterSet(int whichParameter, int newValue);
int sqMIDIParameterGet(int whichParameter);
int sqMIDIPortReadInto(int portNum, int count, int bufferPtr);
int sqMIDIPortWriteFromAt(int portNum, int count, int bufferPtr, int time);


/********************************************************************************/
/********************************************************************************/
/*							additional											*/
/********************************************************************************/
/********************************************************************************/
EXPORT(sqInt) stringToOop(const char *string);

int sqSoftSynthUseDelayUnit(int useOrNot);
int sqSoftSynthUseMatrixReverbUnit(int useOrNot);
int sqSoftSynthUseSoundfont(char *bankPath);
int sqSoftSynthRestoreDLS(void);


/*------------------------------------------------------------------------------*/
/*							AudioUnit											*/
/*------------------------------------------------------------------------------*/
int sqSoftSynthGetAudioUnitParameter(int index);
int sqSoftSynthGetUsedAudioUnitCount(void);
int sqSoftSynthGetAudioUnitCount(void);
int sqSoftSynthGetAudioComponentDescriptioninto(int index, sqInt rcvr);
int sqSoftSynthGetAudioUnitsubTypemanufacturerinto(unsigned long componentType, unsigned long componentSubType, unsigned long componentManufacturer, sqInt rcvr);

/*------------------------------------------------------------------------------*/
/*							AUGraph												*/
/*------------------------------------------------------------------------------*/
int sqSoftSynthGetAUGraphNumberOfInteractions(void);
int sqSoftSynthGetAUGraphInteractionInfointo(int index, sqInt rcvr);
int sqSoftSynthGetAudioUnitFromAUNodeintodescription(signed long node, sqInt audioUnitOop, sqInt audioComponentDescriptionOop);
int sqSoftSynthEditAudioUnitFromAUNode(signed long node);
int sqSoftSynthReplaceAUNodewith(signed long oldNode, sqInt newComponentDescriptionOop);
int sqSoftSynthRemoveAUNode(signed long node);
int sqSoftSynthAddComponentafter(sqInt newComponentDescriptionOop, signed long node);

/*------------------------------------------------------------------------------*/
/*							AudioDevice											*/
/*------------------------------------------------------------------------------*/
int sqSoftSynthGetAudioDeviceCount(void);
int sqSoftSynthPreambleGetAudioDeviceName(int direction);
const char *sqSoftSynthAudioDeviceNameOfdirection(int index, int direction);
int sqSoftSynthPostscriptGetAudioDeviceName(int direction);
const char *sqSoftSynthCurrentOutputDeviceName(void);
int sqSoftSynthSetAudioDeviceNameddirection(const char *deviceName, int direction);


/********************************************************************************/
/********************************************************************************/
/*							for test/debug 										*/
/********************************************************************************/
/********************************************************************************/
int sqSoftSynthPrintUsedAudioUnitParameter(void);


/********************************************************************************/
/*							callback	 										*/
/********************************************************************************/
/********************************************************************************/
#ifdef	__USE_CALLBACK__
int callbackStart(int);
int callbackStop(void);
#endif	//__USE_CALLBACK__

