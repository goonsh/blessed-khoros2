/*
 *  sqPortMidiMac.h
 *  PortMidiPlugin
 *
 *  Created by NISHIHARA Satoshi on 12/01/01.
 *  Copyright 2012 goonsh@gmail.com. All rights reserved.
 *
 */

#ifdef __MACTYPES__

#include <CoreFoundation/CoreFoundation.h>
#include <Carbon/Carbon.h> // kAlertStopAlert

#define PACKET_BUFFER_SIZE 1024
#define SYSEX_BUFFER_SIZE 128

#define MAXSTRINGSIZE 1024

void debugAlert(char *string);
void post(const char *fmt, ...);
void debugWrite(const char *fmt, ...);
	
#endif // __MACTYPES__
