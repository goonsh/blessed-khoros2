/*
 *  sqMacVirtualPort.h
 *  VirtualPortPlugin
 *
 *  Created by NISHIHARA Satoshi on 12/02/03.
 *  Copyright 2012 goonsh@gmail.com. All rights reserved.
 *
 */

#include "CoreMIDI/MIDIServices.h"

#ifdef __USE_VIRTUAL_PORTS__

void vp_initialize(void);
void vp_terminate(void);
void closingModule(void);
void setExternalFunctionAddress(void);
void clearExternalFunctionAddress(void);
OSStatus vp_write(const MIDIPacketList *packetList);
void set_transmit_through(int use);
int get_transmit_through(void);
int virtualPortIsSupported(void);
int virtualPortIsAvailable(void);

/* here is a templates */

/* templates:
 
 if the envrironment does not allow for the establishment of virtual input and 
 output MIDI ports to which other software clients can connect, use this templates
 for NOOP functions. 

 void vp_initialize(void) {}
 void vp_terminate(void) {}
 void closingModule(void) {}
 void void setExternalFunctionAddress(void) {}
 void clearExternalFunctionAddress(void) {}
 OSStatus vp_write(const MIDIPacketList *packetList) {return 0;}
 void set_transmit_through(int use) {}
 int get_transmit_through(void) {return 0;}
 int virtualPortIsSupported(void) {return 0;}
 int virtualPortIsAvailable(void) {return 0;}
 int getUnsignedLongLongOop(int32_t value) {return 0;}
 int setUnsignedLongLongOop(int32_t oop, int32_t *value) {return 0;}

 */

#endif // __USE_VIRTUAL_PORTS__
