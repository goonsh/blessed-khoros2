/*
 *  sqMacVirtualPort.c
 *  PortMidiPlugin
 *
 *  Created by NISHIHARA Satoshi on 12/02/03.
 *  Copyright 2012 goonsh@gmail.com. All rights reserved.
 *
 */

/* 
 * by Gary P. Scavone, author of RtMidi: realtime MIDI i/o C++ classes.
 *
 * http://www.music.mcgill.ca/~gary/rtmidi/index.html#virtual
 *
 *
 * The Linux ALSA and Macintosh CoreMIDI APIs allow for the
 * establishment of virtual input and output MIDI ports to which other
 * software clients can connect. 
 *
 */
/*
 * Any messages sent by PortMidiPlugin will also be transmitted through 
 * an open virtual output port.
 * If a virtual input port is open, the callback function will be invoked 
 * when messages arrive via that port. And signal to semaphore, by the 
 * index of callback semaphore.
 * No notification is provided for the establishment of a client connection 
 * via a virtual port.
 */

//#include <Gestalt.h>

/*	interpreterProxy->signalSemaphoreWithIndex(callbackSemaphoreIndex); */
#include "sq.h"

#include "sqMacVirtualPort.h"
#include "sqMacDebug.h"
#include "portmidi.h"

/***************************************************************
 * modified:
 * Author: goonsh@gmail.com
 * Feb, 2012.
 * experimentaly virtual port support.
 * __USE_VIRTUAL_PORTS__
 ***************************************************************/

#ifdef __USE_VIRTUAL_PORTS__

#define BADARGUMENT -410752

static void (*externalFunc)(void) = 0;
static char *externalPluginName = "VirtualPortPlugin";
static Boolean transmit_through = true;

extern struct VirtualMachine *interpreterProxy;

extern unsigned int composeUnsignedLongLongOopLowHigh(const unsigned long lowWord, const unsigned long highWord);
extern int decomposeUnsignedLongLongOopLowHigh(sqInt oop, unsigned int *lowWordPtr, unsigned int *highWordPtr);

static void *lookupFunctionAddressfrom(char *functionName, char *pluginName) {
	void (*funcAddress)(void) = 0;
	
	funcAddress = interpreterProxy->ioLoadFunctionFrom(functionName, pluginName);
	if (funcAddress == 0) {
		funcAddress = interpreterProxy->ioLoadFunctionFrom(functionName, "");
	}
	return funcAddress;
}


void setExternalFunctionAddress(void) {
	externalFunc = lookupFunctionAddressfrom("writeVirtualOutputPortFrom", externalPluginName);
}

void clearExternalFunctionAddress(void) {
	externalFunc = 0;
}

void set_transmit_through(int use) {
	transmit_through = use == true;
}

int get_transmit_through(void) {
	return transmit_through;
}

int virtualPortIsSupported(void) {
	return (int) interpreterProxy->ioLoadFunctionFrom("getMIDIClientRef", "PortMidiPlugin");;
}

int virtualPortIsAvailable(void) {
	if (externalFunc != 0) {
		void (*virtualPortIsSupportedFuncAddress)(void) = 0;
		virtualPortIsSupportedFuncAddress = lookupFunctionAddressfrom("virtualPortIsSupported", externalPluginName);
		if (virtualPortIsSupportedFuncAddress != 0) {
			return ((Boolean (*) (void)) virtualPortIsSupportedFuncAddress)();
		}
	}
	return 0;
}

OSStatus vp_write(const MIDIPacketList *packetList) {
	OSStatus result = noErr;

	if (transmit_through && (externalFunc != 0)) {
		result = ((OSStatus (*) (const MIDIPacketList *packetList)) externalFunc)(packetList);
#ifdef _sqMacVirtualPort_debug_
		debugWrite("sqMacVirtualPort.c>>*writeVirtualOutputPortFrom()=%ld\n", result);
#endif _sqMacVirtualPort_debug_
	}
	return result;
}

void closingModule(void) {
	void (*externalFuncAddress)(void) = 0;
	
	externalFuncAddress = lookupFunctionAddressfrom("clearExternalFunctionAddress", externalPluginName);
	if (externalFuncAddress != 0) {
		((void (*) (void)) externalFuncAddress)();
	}
}

void vp_initialize(void) {
#ifdef _sqMacVirtualPort_debug_
	debugWrite("PortMidiPlugin vp_initialize: writeVirtualOutputPortFrom()=%ld\n", lookupFunctionAddressfrom("writeVirtualOutputPortFrom", externalPluginName));
	debugWrite("PortMidiPlugin vp_initialize: clearExternalFunctionAddress()=%ld\n", lookupFunctionAddressfrom("clearExternalFunctionAddress", externalPluginName));
#endif _sqMacVirtualPort_debug_

	// not static, look up anytime.
	setExternalFunctionAddress();
	if (externalFunc != 0) {
		void (*virtualPortFunc)(void) = 0;
		virtualPortFunc = lookupFunctionAddressfrom("initializeVirtualPorts", externalPluginName);
		if (virtualPortFunc != 0) {
			((int (*) ()) virtualPortFunc)();
		}
	}
}

void vp_terminate(void) {
	// not static, look up anytime.
	void (*virtualPortFunc)(void) = 0;
	virtualPortFunc = lookupFunctionAddressfrom("terminateVirtualPorts", externalPluginName);
	if (virtualPortFunc != 0) {
		((int (*) ()) virtualPortFunc)();
	}
}

#endif // __USE_VIRTUAL_PORTS__
