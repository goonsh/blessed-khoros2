/*
 *  sqMacVirtualPort.h
 *  VirtualPortPlugin
 *
 *  Created by NISHIHARA Satoshi on 12/02/03.
 *  Copyright 2012 goonsh@gmai.com. All rights reserved.
 *
 */

#include <CoreMIDI/MIDIServices.h>
#include <CoreAudio/CoreAudioTypes.h>
#include <CoreAudio/HostTime.h>

#define _VirtualPorts_
#ifdef _VirtualPorts_

void closingModule(void);
Boolean virtualPortIsSupported(void);
int countVirtualOutputPorts(void);
int countVirtualInputPorts(void);
const char *getVirtualPortName(int index);
int closeVirtualPort(int index);
void closeVirtualPorts(void);
int initializeVirtualPorts(void);
int terminateVirtualPorts(void);
int getVirtualPortDirectionality(unsigned index);
OSStatus openVirtualPort(int index, int direction);
int isVirtualPortOpened(int index);

int getFilterOop(unsigned index);
int setFilterOop(unsigned index, int32_t oop);
int getChannelMaskOop(unsigned index);
int setChannelMaskOop(unsigned index, int32_t oop);
	
int callbackStart(int semaphoreIndex);
int callbackStop(void);
int callbackLength(void);
int callbackDestinationIndex(void);
const char *callbackBytes(void);
unsigned int callbackTimeStampOop(void);

int notifyCallbackStart(int semaphoreIndex);
int notifyCallbackStop(void);
const char *notifyCallbackMessage(void);

void clearExternalFunctionAddress(void);
Boolean useNotifyMIDISystemStateChange(void);

/* write to source only, without opened output port */
OSStatus writeVirtualOutputPortAtFrom(int index, int count, int bufferPtr, unsigned long microsecondTime);

/* write to source with opened output port */
OSStatus writeVirtualOutputPortFrom(const MIDIPacketList *packetList);

/* MIDITimeStamp -> UInt64 -> unsigned long long */
unsigned int getMIDIClockOop(void);

#endif // _VirtualPorts_
