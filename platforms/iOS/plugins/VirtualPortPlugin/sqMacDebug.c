/*
 *  sqPortMidiMac.h.c
 *  VirtualPortPlugin
 *
 *  Created by NISHIHARA Satoshi on 12/01/01.
 *  Copyright 2012 goonsh@gmai.com. All rights reserved.
 *
 */


#include <stdio.h>

#include "sqMacDebug.h"

void debugAlert(char *string) {
	CFStringRef cfString = CFStringCreateWithCString(kCFAllocatorDefault, string, kCFStringEncodingUTF8);
	DialogRef theAlert;
	CreateStandardAlert(kAlertStopAlert, cfString, NULL, NULL, &theAlert);
	RunStandardAlert(theAlert, NULL, NULL);
}

void post(const char *fmt, ...) {
	char buf[MAXSTRINGSIZE];
    va_list ap;
    //t_int arg[8];
    //int i;
    va_start(ap, fmt);
    vsnprintf(buf, MAXSTRINGSIZE - 1, fmt, ap);
    va_end(ap);
    strcat(buf, "\n");
	debugAlert(buf);
}

void debugWrite(const char *fmt, ...) {
	FILE *fp;
	char buf[MAXSTRINGSIZE];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, MAXSTRINGSIZE - 1, fmt, ap);
    va_end(ap);
	fp = fopen("debug.txt", "a+");
	fputs(buf, fp);
	fclose(fp);
}
