#import <Cocoa/Cocoa.h>
#include <AudioToolbox/AudioToolbox.h>
#include <AudioUnit/AudioUnit.h>

#import <CoreAudioKit/CoreAudioKit.h>
#import <AudioUnit/AUCocoaUIView.h>

//@implementation AUViewWindowController

static BOOL pluginClassIsValid(Class pluginClass);
NSView *create_cocoa_view(AudioUnit unit);
NSWindow *createCocoaWindow(AudioUnit unit);

static BOOL pluginClassIsValid(Class pluginClass) {
	if ([pluginClass conformsToProtocol: @protocol(AUCocoaUIBase)]) {
		if ( [pluginClass instancesRespondToSelector: @selector(interfaceVersion)] &&
				[pluginClass instancesRespondToSelector: @selector(uiViewForAudioUnit:withSize:)]) {
				printf("YES\n");
			return YES;
		}
	}
	return NO;
}

NSView *create_cocoa_view(AudioUnit unit) {
	NSView *theView = nil;
	UInt32 dataSize = 0;
	Boolean isWritable = 0;
	OSStatus err = AudioUnitGetPropertyInfo(unit,
				kAudioUnitProperty_CocoaUI, kAudioUnitScope_Global, 
				0, &dataSize, &isWritable);

puts("create_cocoa_view1");

	if(err != noErr) {
		//[self error: @"Getting cocoa view info" status: err];
		return nil;
	}

	// If we have the property, then allocate storage for it.
	AudioUnitCocoaViewInfo *cvi = (AudioUnitCocoaViewInfo *) malloc(dataSize);
	err = AudioUnitGetProperty(unit, 
	kAudioUnitProperty_CocoaUI, kAudioUnitScope_Global, 0, cvi, &dataSize);
puts("create_cocoa_view2");

	// Extract useful data.
	unsigned numberOfClasses = (dataSize - sizeof(CFURLRef)) / sizeof(CFStringRef);
	NSString *viewClassName = (NSString *)(cvi->mCocoaAUViewClass[0]);
	NSString *path = (NSString *) (CFURLCopyPath(cvi->mCocoaAUViewBundleLocation));
	NSBundle *viewBundle = [NSBundle bundleWithPath:[path autorelease]];
	Class viewClass = [viewBundle classNamed:viewClassName];
puts("create_cocoa_view3");

	if (pluginClassIsValid(viewClass)) {
puts("pluginClassIsValid");
		id factory = [[[viewClass alloc] init] autorelease];
		theView = [factory uiViewForAudioUnit:unit withSize:NSMakeSize(400, 300)];
	} else {
puts("pluginClassIsInvalid");
	}

	// Delete the cocoa view info stuff.
	if (cvi) {
		int i;
		for (i = 0; i < numberOfClasses; i++)
			CFRelease(cvi->mCocoaAUViewClass[i]);

		CFRelease(cvi->mCocoaAUViewBundleLocation);
		free(cvi);
	}

// puts("create_cocoa_view4");
// 
// 	//
// 	if (theView != nil) {
// puts("theView != nil");
// 		createCocoaWindow(unit);
// 	} else {
// puts("theView == nil");
// 	}
// 	createCocoaWindow(unit);
	//
	return theView;
}

//- (NSWindow *)createCocoaWindow
NSWindow *createCocoaWindow(AudioUnit unit) {
	//if ([self hasCocoaView]) {
		//NSView *res = [self getCocoaView];
		NSView *res = create_cocoa_view(unit);
		//create_cocoa_view();
		if (res) {
			NSWindow *cocoaWindow = [[[NSWindow alloc] initWithContentRect: NSMakeRect(100, 400, [res frame].size.width, [res frame].size.height) styleMask: NSTitledWindowMask | NSClosableWindowMask | NSMiniaturizableWindowMask | NSResizableWindowMask backing:NSBackingStoreBuffered defer:NO] autorelease];
			[cocoaWindow setContentView:res];
printf("createCocoaWindow!!! is here\n");
			return cocoaWindow;
		}
	//}
	return nil;
}

int showWindow2(void) {
	[NSAutoreleasePool new];
	[NSApplication sharedApplication];
	[NSApp setActivationPolicy:NSApplicationActivationPolicyRegular];
	id menubar = [[NSMenu new] autorelease];
	id appMenuItem = [[NSMenuItem new] autorelease];
	[menubar addItem:appMenuItem];
	[NSApp setMainMenu:menubar];
	id appMenu = [[NSMenu new] autorelease];
	id appName = [[NSProcessInfo processInfo] processName];
	id quitTitle = [@"Quit " stringByAppendingString:appName];
	id quitMenuItem = [[[NSMenuItem alloc] initWithTitle:quitTitle
		action:@selector(terminate:) keyEquivalent:@"q"] autorelease];
	[appMenu addItem:quitMenuItem];
	[appMenuItem setSubmenu:appMenu];
	id window = [[[NSWindow alloc] initWithContentRect:NSMakeRect(0, 0, 200, 200)
		styleMask:NSTitledWindowMask backing:NSBackingStoreBuffered defer:NO]
			autorelease];
	[window cascadeTopLeftFromPoint:NSMakePoint(20,20)];
	[window setTitle:appName];
	[window makeKeyAndOrderFront:nil];
	[NSApp activateIgnoringOtherApps:YES];
	[NSApp run];
	return 0;
}
