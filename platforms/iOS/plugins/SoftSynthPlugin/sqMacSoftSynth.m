/****************************************************************************
 *  sqMacSoftSynth.c
 *  SoftSynthPlugin
 *	Yet Another MIDI port for MacOSX.
 *	
 *	Provide Composable Software MIDI support, if your platform provides it. 
 *	
 *	restriction:
 *		MIDI in is not supported, out only.
 *		currently, Mac OS X, Core-Audio only.
 *	
 *	music device is DLSMusicDevice, a virtual instrument unit that lets you play 
 *	MIDI data using sound banks in the SoundFont or Downloadable Sounds (DLS) format. 
 *	2 effects support: 
 *		AUDelay: a delay unit.
 *		AUMatrixReverb: a reverberation unit that allows you to specify spatial 
 *		characteristics, such as size of room, material absorption characteristics, and so on.
 *
 *  Created by NISHIHARA Satoshi on 13/07/18.
 *  Copyright 2013 goonsh@gmail.com. All rights reserved.
 *
 ****************************************************************************/

#include "sq.h"

#include <CoreServices/CoreServices.h>
#include <CoreAudio/CoreAudio.h>
#include <AudioUnit/AudioUnit.h>
#include <AudioToolbox/AUGraph.h>
#include <AudioToolbox/AudioToolbox.h> 		//for AUGraph
#include <AudioUnit/AudioUnitProperties.h>	//for preset
#include <AudioUnit/AudioUnitCarbonView.h>	//for view
#include <AssertMacros.h>
#include <limits.h>

#include "SoftSynthPlugin.h"
#include "debug.h"
#import "NativeUIController.h"

extern struct VirtualMachine *interpreterProxy;

/* Quicktime MIDI note allocator and channels */
#define FIRST_DRUM_KIT (16385 - 1)
#define DRUM_CHANNEL (10 - 1)

/* macros */
#if TARGET_RT_BIG_ENDIAN
#   define FourCC2Str(fourcc) (const char[]){*((char*)&fourcc), *(((char*)&fourcc)+1), *(((char*)&fourcc)+2), *(((char*)&fourcc)+3),0}
#else
#   define FourCC2Str(fourcc) (const char[]){*(((char*)&fourcc)+3), *(((char*)&fourcc)+2), *(((char*)&fourcc)+1), *(((char*)&fourcc)+0),0}
#endif


/*----------------------------------------------------------------------------
	//AudioUnitParameters.h

	 // Music Device
	 // Parameters for the DLSMusicDevice unit - defined and reported in the global scope
	 enum {
	 // Global, Cents, -1200, 1200, 0
	 kMusicDeviceParam_Tuning 	= 0,
	 
	 // Global, dB, -120->40, 0
	 kMusicDeviceParam_Volume	= 1,
	 
	 // Global, dB, -120->40, 0
	 kMusicDeviceParam_ReverbVolume	= 2
	 };

	 // Parameters for the AUDelay unit
	 enum {
	 // Global, EqPow Crossfade, 0->100, 50
	 kDelayParam_WetDryMix 				= 0,
	 
	 // Global, Secs, 0->2, 1
	 kDelayParam_DelayTime				= 1,
	 
	 // Global, Percent, -100->100, 50
	 kDelayParam_Feedback 				= 2,
	 
	 // Global, Hz, 10->(SampleRate/2), 15000
	 kDelayParam_LopassCutoff	 		= 3
	 };

	 // Parameters for the AUMatrixReverb unit
	 enum {
	 // Global, EqPow CrossFade, 0->100, 100
	 kReverbParam_DryWetMix 							= 0,
	 
	 // Global, EqPow CrossFade, 0->100, 50
	 kReverbParam_SmallLargeMix						= 1,
	 
	 // Global, Secs, 0.005->0.020, 0.06
	 kReverbParam_SmallSize							= 2,
	 
	 // Global, Secs, 0.4->10.0, 3.07
	 kReverbParam_LargeSize							= 3,
	 
	 // Global, Secs, 0.001->0.03, 0.025
	 kReverbParam_PreDelay							= 4,
	 
	 // Global, Secs, 0.001->0.1, 0.035
	 kReverbParam_LargeDelay							= 5,
	 
	 // Global, Genr, 0->1, 0.28
	 kReverbParam_SmallDensity						= 6,
	 
	 // Global, Genr, 0->1, 0.82
	 kReverbParam_LargeDensity						= 7,
	 
	 // Global, Genr, 0->1, 0.3
	 kReverbParam_LargeDelayRange					= 8,
	 
	 // Global, Genr, 0.1->1, 0.96
	 kReverbParam_SmallBrightness					= 9,
	 
	 // Global, Genr, 0.1->1, 0.49
	 kReverbParam_LargeBrightness					= 10,
	 
	 // Global, Genr, 0->1 0.5
	 kReverbParam_SmallDelayRange					= 11,
	 
	 // Global, Hz, 0.001->2.0, 1.0
	 kReverbParam_ModulationRate						= 12,
	 
	 // Global, Genr, 0.0 -> 1.0, 0.2
	 kReverbParam_ModulationDepth					= 13,
	 
	 // Global, Hertz, 10.0 -> 20000.0, 800.0
	 kReverbParam_FilterFrequency					= 14,
	 
	 // Global, Octaves, 0.05 -> 4.0, 3.0
	 kReverbParam_FilterBandwidth					= 15,
	 
	 // Global, Decibels, -18.0 -> +18.0, 0.0
	 kReverbParam_FilterGain							= 16
	 };

-----------------------------------------------------------------------------*/

/* audio device */
enum {
	IN = 0,
	OUT,
	BOTH
};

/* Initial instruments: drums on channel 10, piano on all other channels */
//int channelInstrument[16] = {
//	1, 1, 1, 1, 1, 1, 1, 1, 1, FIRST_DRUM_KIT, 1, 1, 1, 1, 1, 1};

/* Quicktime MIDI parser state */
/* same as sqMacMIDI.c */
enum {idle, want1of2, want2of2, want1of1, sysExclusive};
static int state = idle;
static int argByte1 = 0;
static int argByte2 = 0;
static int lastCmdByte = 0;

/* number of argument bytes for each MIDI command */
static char argumentBytes[128] = {
	2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
	2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
	2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
	2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
	3, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

// some MIDI constants:
enum {
	kMidiMessage_ControlChange 		= 0xB,
	kMidiMessage_ProgramChange 		= 0xC,
	kMidiMessage_BankMSBControl 	= 0,
	kMidiMessage_BankLSBControl		= 32,
	kMidiMessage_NoteOn 			= 0x9
};


/*----------------------------------------------------------------------------
	statics	variables
-----------------------------------------------------------------------------*/
/* augraph */
static AUGraph graph = NULL;
static AudioUnit synthUnit = NULL;
static AUNode synthNode = 0;
static AUNode delayNode = 0;
static AUNode reverbNode = 0;
static AUNode outNode = 0;

static MusicDeviceInstrumentID channelInstrument[16];
static int usesReverb = false;
static int usesDelay = false;

/* dls name */
static const char *portName = "QuickTime Music Synthesizer";

static const CFStringEncoding encoding = kCFStringEncodingUTF8;	//kCFStringEncodingMacRoman;

#ifdef	__USE_CALLBACK__
/* callback */
static int callbackSemaphoreIndex = 0;
#endif	//__USE_CALLBACK__


/*----------------------------------------------------------------------------
	statics	function prototypes
-----------------------------------------------------------------------------*/
/*** MIDI Support Functions ***/
static void performMIDICmd(int cmdByte, int arg1, int arg2);
/* same as sqMacMIDI.c */
static void processMIDIByte(int aByte);
static void startMIDICommand(int cmdByte);

static void initChannelInstrument(void);

/* module initialization/shutdown */
static OSStatus	createAUGraph(AUGraph *outGraph, AudioUnit *outSynth);
static OSStatus destoryAUGGraph(AUGraph graph);
static void initDelay(AUGraph graph, AUNode delayNode);
static void initMatrixReverb(AUGraph graph, AUNode reverbNode);

/* presets */
static CFArrayRef getPreset(AudioUnit matrixReverbAudioUnit);
static OSStatus setPreset(UInt32 presetNumber, AudioUnit matrixReverbAudioUnit);

/* audio component/unit */
static sqInt getAudioComponentDescription(AudioComponent component, sqInt oop);
static int isAudioUnit(AudioComponentDescription cd);
static AudioUnit getAudioUnit(AudioUnit unit, sqInt rcvr);
static CFStringRef getAudioComponentName(AudioComponent component);

static int hasCocoaUI(AudioUnit unit);
static int hasCarbonUI(AudioUnit unit);

/* augraph */
static AudioUnit getAGetAudioUnitFromAUNode(AUNode node);
static OSStatus replaceNodeInAUGraph(AUGraph graph, AUNode oldNode, AudioComponentDescription newAudioComponentDescription);
static OSStatus removeAUNode(AUGraph graph, AUNode node);
static OSStatus addComponentafter(AUGraph graph, AudioComponentDescription newAudioComponentDescription, AUNode node);

/* audio device */
static CFArrayRef CreateAudioDeviceArray(void);
static int getDirection(AudioDeviceID device);
static CFArrayRef getAudioDevices(int type);
static Boolean setOutputDevice(AudioDeviceID deviceID, AUNode halNode, AUGraph theGraph);
static void audioDeviceNames(int direction);
static UInt32 propertySizeOfAudioDevices(void);

/* print input/output bus numbers */
static void getBusNumber(AudioComponent component);

/* print Parameters of AudioUnit */
static void getAudioUnitParameter(AudioUnit unit);
static void getAudioUnitParameterFromNode(AUNode node);

/* utilities */
static AudioComponentDescription audioComponentDescriptionFromOop(sqInt descriptionOop);
static int nsAlert(char *cmessageText, char *cinformativeText);

/* externs from VM */


/********************************************************************************/
/********************************************************************************/
/*							platform specific midi functions					*/
/*							prescribed in　sqMacMIDI.c							*/
/********************************************************************************/
/********************************************************************************/

/*****************************************************************************
	midiInit
	
	initialize/shutdown
	@param none
	@return noErr or not
******************************************************************************/
int midiInit() { 
	OSStatus result;

	result = createAUGraph(&graph, &synthUnit);
	if (result != noErr) {
		destoryAUGGraph(graph);
	}
#ifdef __DEBUG__
	CAShow(graph);
#endif //__DEBUG__
	
    return result == noErr; 
}


/*****************************************************************************
	midiShutdown
	
	initialize/shutdown
	@param none
	@return noErr or not
******************************************************************************/
int midiShutdown(void) {
	return destoryAUGGraph(graph) == noErr;
}


/*****************************************************************************
	sqMIDIGetClock
	
	same as sqMacMIDI.c
	@param none
	@return the value of the Squeak millisecond clock.
******************************************************************************/
int sqMIDIGetClock(void) {
/* Return the current value of the clock used to schedule MIDI events.
   The MIDI clock is assumed to wrap at or before half the maximum
   positive SmallInteger value. This allows events to be scheduled
   into the future without overflowing into LargePositiveIntegers. 
   This implementation does not support event scheduling, so it
   just returns the value of the Squeak millisecond clock. */

	return interpreterProxy->ioMicroMSecs();
}


/*****************************************************************************
	sqMIDIGetPortCount
	
	currenyly single graph = 1 port
	@param none
	@return 1 when global graph is opened or 0.
******************************************************************************/
int sqMIDIGetPortCount(void) {
/* Return the number of available MIDI interfaces, including both
   hardware ports and software entities that act like ports. Ports
   are numbered from 0 to N-1, where N is the number returned by this
   primitive. */
	
	if (graph != NULL) {
		CAShow(graph);
		return 1;
	} else {
		return 0;
	}
}


/*****************************************************************************
	sqMIDIGetPortDirectionality
	
	output only
	@param portNum ignored
	@return fixed value 2
******************************************************************************/
int sqMIDIGetPortDirectionality(int portNum) {
/* Return an integer indicating the directionality of the given
   port where: 1 = input, 2 = output, 3 = bidirectional. Fail if
   there is no port of the given number. */
	
	return 2;
}


/*****************************************************************************
	sqMIDIGetPortName
	
	answer fixed portname
	@param portNum ignored
	@param buffer of name
	@param length of buffer
	@return fixed value 2
	@TODO it should answer soundfont filename if loaded.
******************************************************************************/
int sqMIDIGetPortName(int portNum, int namePtr, int length) {
/* Copy the name of the given MIDI port into the string at the given
   address. Copy at most length characters, and return the number of
   characters copied. Fail if there is no port of the given number.*/

	int count;
	
	count = strlen(portName);
	if (count > length) count = length;
	memcpy((void *) namePtr, portName, count);
	return count;
}


/*****************************************************************************
	sqMIDIClosePort
	
	stop augraph
	@param portNum ignored
	@return noErr or not
******************************************************************************/
int sqMIDIClosePort(int portNum) {
/* Close the given MIDI port. Do nothing if the port is not open.
   Fail if there is no port of the given number.*/

	OSStatus result = kHIDSuccess;
	Boolean outIsRunning;
	
	AUGraphIsRunning(graph, &outIsRunning);
	if (outIsRunning) {
		require_noerr(result = AUGraphStop(graph), home);	
	}
	return result;

  home:
	midiShutdown();
	return result == noErr;
}


/*****************************************************************************
	sqMIDIOpenPort
	
	start augrpah
	@param portNum ignored
	@param portNum ignored
	@param index of semaphore ignored
	@param interfaceClockRate ignored
	@return 0, primitiveMIDIOpenPort does not receive return value
	@TODO require rewrite.
******************************************************************************/
int sqMIDIOpenPort(int portNum, int readSemaIndex, int interfaceClockRate) {
/* Open the given port, if possible. If non-zero, readSemaphoreIndex
   specifies the index in the external objects array of a semaphore
   to be signalled when incoming MIDI data is available. Note that
   not all implementations support read semaphores (this one does
   not); see sqMIDICanUseSemaphore. The interfaceClockRate parameter
   specifies the clock speed for an external MIDI interface
   adaptor on platforms that use such adaptors (e.g., Macintosh).
   Fail if there is no port of the given number.*/

	OSStatus result;
	UInt8 midiChannelInUse = 0;

	if (!graph) {
		midiInit();
	}
	if (false) return 0;
	//require_noerr(result = AUGraphInitialize(graph), home);
	require_noerr(result = MusicDeviceMIDIEvent(synthUnit, 
								kMidiMessage_ControlChange << 4 | midiChannelInUse, 
								kMidiMessage_BankMSBControl, 0,
								0/*sample offset*/), home);
	require_noerr(result = MusicDeviceMIDIEvent(synthUnit, 
								kMidiMessage_ProgramChange << 4 | midiChannelInUse, 
								0/*prog change num*/, 0,
								0/*sample offset*/), home);
	// Start playing
	require_noerr(result = AUGraphStart(graph), home);	
	return 0;

home:
	midiShutdown();
	if (result != noErr) {
		fprintf(stderr, "failed to open port: [%ld]\n", result);
	}
	return 0;
}


/*****************************************************************************
	sqMIDIParameterSet
	
	do nothing, not called
	@param which parameter ignored
	@param new value to set
	@return 0
******************************************************************************/
int sqMIDIParameterSet(int whichParameter, int newValue) {
	return 0;
}


/*****************************************************************************
	sqMIDIParameterGet
	
	do nothing, not called
	@param which parameter ignored
	@return 0
******************************************************************************/
int sqMIDIParameterGet(int whichParameter) {
	return 0;
}


/*****************************************************************************
	sqMIDIPortReadInto
	
	currently not supported
	@param portNum ignored
	@param size to read ignored
	@param pointer to buffer ignored
	@return 0
******************************************************************************/
int sqMIDIPortReadInto(int portNum, int count, int bufferPtr) {
/* bufferPtr is the address of the first byte of a Smalltalk
   ByteArray of the given length. Copy up to (length - 4) bytes
   of incoming MIDI data into that buffer, preceded by a 4-byte
   timestamp in the units of the MIDI clock, most significant byte
   first. Implementations that do not support timestamping of
   incoming data as it arrives (see sqMIDIHasInputClock) simply
   set the timestamp to the value of the MIDI clock when this
   function is called. Return the total number of bytes read,
   including the timestamp bytes. Return zero if no data is
   available. Fail if the buffer is shorter than five bytes,
   since there must be enough room for the timestamp plus at
   least one data byte. */

	return 0;
}


/*****************************************************************************
	sqMIDIPortWriteFromAt
	
	@param portNum ignored
	@param pointer to buffer ignored
	@param latency ignored
	@return size of bytes to send
	@TODOrequire sys_ex handling
******************************************************************************/
int sqMIDIPortWriteFromAt(int portNum, int count, int bufferPtr, int time) {
/* bufferPtr is the address of the first byte of a Smalltalk
   ByteArray of the given length. Send its contents to the given
   port when the MIDI clock reaches the given time. If time equals
   zero, then send the data immediately. Implementations that do
   not support a timestamped output queue, such as this one, always
   send the data immediately; see sqMIDIHasBuffer. */
	
	int i;
	unsigned char *bytePtr;
	
	bytePtr = (unsigned char *) bufferPtr;
	for(i = 0; i < count; i++) {
		processMIDIByte(*bytePtr++);
	}
	return count;
}


/*----------------------------------------------------------------------------
	performMIDICmd
	
	that's all for now, throw all bytes to MusicDeviceMIDIEvent
	@param cmdByte status byte
	@param 1st data byte
	@param 2nd data byte
	@return void
	@TODO require sys_ex
-----------------------------------------------------------------------------*/
static void performMIDICmd(int cmdByte, int arg1, int arg2) {
	flag("TODO");
	MusicDeviceMIDIEvent(synthUnit, cmdByte, arg1, arg2, 0);
	//printf(">>> 0x%02X 0x%02X 0x%02X\n", cmdByte, arg1, arg2);
}


/*----------------------------------------------------------------------------
	processMIDIByte
	
	same as sqMacMIDI.c
	@param byte
	@return void
-----------------------------------------------------------------------------*/
static void processMIDIByte(int aByte) {
	/* Process the given incoming MIDI byte and perform any completed commands. */

	if (aByte > 247) return;  /* skip all real-time messages */

	switch (state) {
	case idle:
		if (aByte >= 128) {
			/* start a new command using the action table */
			startMIDICommand(aByte);
		} else {
			/* data byte arrived in idle state: use running status if possible */
			if (lastCmdByte == 0) {
				return;  /* last command byte is not defined; just skip this byte */
			} else {
				/* process this data as if it had the last command byte in front of it */
				startMIDICommand(lastCmdByte);
				/* the previous line put us into a new state; we now do a recursive
			   	   call to process the data byte in this new state. */
				processMIDIByte(aByte);
				return;
			}
		}
		break;
	case want1of2:
		argByte1 = aByte;
		state = want2of2;
		break;
	case want2of2:
		argByte2 = aByte;
		performMIDICmd(lastCmdByte, argByte1, argByte2);
		state = idle;
		break;
	case want1of1:
		argByte1 = aByte;
		performMIDICmd(lastCmdByte, argByte1, 0);
		state = idle;
		break;
	case sysExclusive:
		if (aByte < 128) {
			/* skip a system exclusive data byte */
		} else {
			if (aByte < 248) {
				/* a system exclusive message can be terminated by any non-real-time command byte */
				state = idle;
				if (aByte != 247) {
					processMIDIByte(aByte);	/* if not endSysExclusive, byte is the start the next command */
				}
			}
		}
		break;
	}
}


/*----------------------------------------------------------------------------
	startMIDICommand
	
	same as sqMacMIDI.c
	@param cmdByte status byte
	@return void
-----------------------------------------------------------------------------*/
static void startMIDICommand(int cmdByte) {
	/* Start processing a MIDI message beginning with the given command byte. */

	int argCount;

	argCount = argumentBytes[cmdByte - 128];
	switch (argCount) {
	case 0:						/* start a zero argument command (e.g., a real-time message) */
		/* Stay in the current state and don't change active status.
		   Real-time messages may arrive between data bytes without disruption. */
		performMIDICmd(cmdByte, 0, 0);
		break;
	case 1:						/* start a one argument command */
		lastCmdByte = cmdByte;
		state = want1of1;
		break;
	case 2:						/* start a two argument command */
		lastCmdByte = cmdByte;
		state = want1of2;
		break;
	case 3:						/* start a variable length 'system exclusive' command */
		/* a system exclusive command clears running status */
		lastCmdByte = 0;
		state = sysExclusive;
		break;
	}
}


static void initChannelInstrument(void) {
	int i;
	
	for (i = 0; i < 16; i++) {
		channelInstrument[i] = 0;
	}
	channelInstrument[DRUM_CHANNEL] = FIRST_DRUM_KIT;
}

/********************************************************************************/
/********************************************************************************/
/*							additional											*/
/********************************************************************************/
/********************************************************************************/

/********************************************************************************/
/********************************************************************************/
/*							AUGraph												*/
/********************************************************************************/
/********************************************************************************/

/*----------------------------------------------------------------------------
	initDelay
	
	initialize delay unit
	@param AUGraph
	@param AUNode delayNode
	@return void 
	@TODO to be deprecated
-----------------------------------------------------------------------------*/
static void initDelay(AUGraph graph, AUNode delayNode) {
	OSStatus result;
	AudioUnit delayAudioUnit;
	float wetDryMix = 0.0;
	int delayTime = 0;
	int feedback = 0;
	int lopasCutoff = 10;
	
	AUGraphNodeInfo(graph, delayNode, 0, &delayAudioUnit);
	result = AudioUnitSetParameter(
					delayAudioUnit,
					kDelayParam_WetDryMix, 
					kAudioUnitScope_Global,
					0, 
					wetDryMix, 0);
	result = AudioUnitSetParameter(
					delayAudioUnit,
					kDelayParam_DelayTime, 
					kAudioUnitScope_Global,
					0, 
					delayTime, 0);
	result = AudioUnitSetParameter(
					delayAudioUnit,
					kDelayParam_Feedback, 
					kAudioUnitScope_Global,
					0, 
					feedback, 0);
	result = AudioUnitSetParameter(
					delayAudioUnit,
					kDelayParam_LopassCutoff, 
					kAudioUnitScope_Global,
					0, 
					lopasCutoff, 0);
	
}


/*----------------------------------------------------------------------------
	initMatrixReverb
	
	initialize MatrixReverb unit
	@param AUGraph
	@param AUNode reverbNode
	@return void 
	@TODO to be deprecated
-----------------------------------------------------------------------------*/
static void initMatrixReverb(AUGraph graph, AUNode reverbNode) {
	OSStatus result;
	AudioUnit matrixReverbAudioUnit;
	float dryWetMix = 0.0;
	int smallLargeMix = 0;
	int feedback = 0;
	int lopasCutoff = 10;
	
	AUGraphNodeInfo(graph, reverbNode, 0, &matrixReverbAudioUnit);
	result = AudioUnitSetParameter(
					matrixReverbAudioUnit,
					kReverbParam_DryWetMix, 
					kAudioUnitScope_Global,
					0, 
					dryWetMix, 0);
#ifdef __DEBUG__
 	getPreset(matrixReverbAudioUnit);
#endif //__DEBUG__
}


/*----------------------------------------------------------------------------
	setPreset
	
	set preset for given autio unit
	@param UInt32 presetNumber answerd by getPreset(audioUnit)
	@param AudioUnit audioUnit
	@return OSStatus 
-----------------------------------------------------------------------------*/
static OSStatus setPreset(UInt32 presetNumber, AudioUnit audioUnit) {
	AUPreset newPreset;
	OSStatus result;
	
	newPreset.presetNumber = presetNumber;
	result = AudioUnitSetProperty(audioUnit,
					kAudioUnitProperty_PresentPreset,
					kAudioUnitScope_Global,
					0,
					&newPreset,
					sizeof(AUPreset));
	return result;
}


/*----------------------------------------------------------------------------
	getPreset
	
	get preset for given autio unit
	@param AudioUnit audioUnit
	@return CFArrayRef array of presets
-----------------------------------------------------------------------------*/
static CFArrayRef getPreset(AudioUnit audioUnit) {
	CFArrayRef presets = NULL;
	UInt32 propertySize = sizeof(presets);
	AudioUnitGetProperty(audioUnit,
					kAudioUnitProperty_FactoryPresets,
					kAudioUnitScope_Global,
					0,
					&presets,
					&propertySize);
#ifdef __DEBUG__
	{
		int count, i;

		count = CFArrayGetCount(presets);
		for (i = 0; i < count; i++) {
			AUPreset *preset = (AUPreset *) CFArrayGetValueAtIndex(presets, i);
			printf("%ld: %s\n", preset->presetNumber, CFStringGetCStringPtr(preset->presetName, encoding));
		}
	}
#endif //__DEBUG__
	return presets;
}


/*----------------------------------------------------------------------------
	createAUGraph
	
	create augraph for initializing, but not start graph.
	start at opening
	@param AUGraph *outGraph
	@param AudioUnit *outSynth
	@return OSStatus
-----------------------------------------------------------------------------*/
static OSStatus	createAUGraph(AUGraph *outGraph, AudioUnit *outSynth) {
	OSStatus result;
	AudioComponentDescription cd;
	int i;
	
	// Create the graph
	require_noerr(result = NewAUGraph(outGraph), home);

	// Open the DLS Synth
	cd.componentType = kAudioUnitType_MusicDevice;
	cd.componentSubType = kAudioUnitSubType_DLSSynth;
	cd.componentManufacturer = kAudioUnitManufacturer_Apple;
	cd.componentFlags = 0;
	cd.componentFlagsMask = 0;
	require_noerr(result = AUGraphAddNode(*outGraph, &cd, &synthNode), home);

	initChannelInstrument();

	// Open Delay
	cd.componentType = kAudioUnitType_Effect;
	cd.componentSubType = kAudioUnitSubType_Delay;  
	cd.componentManufacturer = kAudioUnitManufacturer_Apple;
	cd.componentFlags = 0;
	cd.componentFlagsMask = 0;
	require_noerr(result = AUGraphAddNode(*outGraph, &cd, &delayNode), home);

	// Open MatrixReverb
	cd.componentType = kAudioUnitType_Effect;
	cd.componentSubType = kAudioUnitSubType_MatrixReverb;  
	cd.componentManufacturer = kAudioUnitManufacturer_Apple;
	cd.componentFlags = 0;
	cd.componentFlagsMask = 0;
	require_noerr(result = AUGraphAddNode(*outGraph, &cd, &reverbNode), home);

	// Open the output device
	cd.componentType = kAudioUnitType_Output;
	//cd.componentSubType = kAudioUnitSubType_DefaultOutput;
	cd.componentSubType = kAudioUnitSubType_HALOutput;
	cd.componentManufacturer = kAudioUnitManufacturer_Apple;
	cd.componentFlags = 0;
	cd.componentFlagsMask = 0;
	require_noerr(result = AUGraphAddNode(*outGraph, &cd, &outNode), home);
	
	// Connect the devices up
	require_noerr(result = AUGraphConnectNodeInput(*outGraph, synthNode, 0, delayNode, 0), home);
	require_noerr(result = AUGraphConnectNodeInput(*outGraph, delayNode, 0, reverbNode, 0), home);
	require_noerr(result = AUGraphConnectNodeInput(*outGraph, reverbNode, 0, outNode, 0), home);
	require_noerr(result = AUGraphUpdate(*outGraph, NULL), home);
	
	require_noerr(result = AUGraphOpen(*outGraph), home);
	require_noerr(result = AUGraphInitialize(*outGraph), home);
	
	// ok we're good to go - get the Synth Unit...
	require_noerr(result = AUGraphNodeInfo(*outGraph, synthNode, 0, outSynth), home);

	// Turn off the reverb on the synth
	require_noerr(result = AudioUnitSetProperty(
					*outSynth,
					kMusicDeviceProperty_UsesInternalReverb, 
					kAudioUnitScope_Global,
					0,
					&usesReverb, sizeof(usesReverb)
	), home);
	
	initDelay(*outGraph, delayNode);
	initMatrixReverb(*outGraph, reverbNode);

	return result;

home:
	midiShutdown();
	if (result != noErr) {
		fprintf(stderr, "failed to create augraph: [%ld]\n", result);
	}
	return result;
}


/*----------------------------------------------------------------------------
	destoryAUGGraph
	
	destroy augraph for shutdown
	stop at closing
	@param AUGraph graph
	@return OSStatus
-----------------------------------------------------------------------------*/
static OSStatus destoryAUGGraph(AUGraph graph) {
	OSStatus result = kHIDSuccess;
	if (graph) {
		Boolean outIsRunning;
		AUGraphIsRunning(graph, &outIsRunning);
		if (outIsRunning) {
			require_noerr(result = AUGraphStop(graph), home);	
		}
		require_noerr(result = AUGraphUninitialize(graph), home);
		require_noerr(result = AUGraphClose(graph), home);
		require_noerr(result = DisposeAUGraph(graph), home);;
	}
	
home:
	graph = NULL;
	if (result != noErr) {
		fprintf(stderr, "destoryAUGGraph faild: %ld\n", result);
	}
	return result;
}


/*****************************************************************************
	sqSoftSynthUseDelayUnit
	
	@param use delay or not
	@return void
	@TODO to be removed
******************************************************************************/
int sqSoftSynthUseDelayUnit(int useOrNot) {
	usesDelay = useOrNot == true;
	return 0;
}


/*****************************************************************************
	sqSoftSynthUseMatrixReverbUnit

	@param use MatrixReverb or not
	@return void
	@TODO to be removed
******************************************************************************/
int sqSoftSynthUseMatrixReverbUnit(int useOrNot) {
	usesReverb = useOrNot == true;
	return 0;
}


/*****************************************************************************
	sqSoftSynthUseSoundfont
	
	use Soundfont. fulpath required. 
	@param fullpath for soundfont file
	@return noErr or not
******************************************************************************/
int sqSoftSynthUseSoundfont(char *bankPath) {
	OSStatus result = kHIDSuccess;
	FSRef fsRef;
	int i;
	
	require_noerr(result = FSPathMakeRef((const UInt8*)bankPath, &fsRef, 0), home);
	AUGraphStop(graph);
	require_noerr(result = AudioUnitSetProperty(synthUnit,
					kMusicDeviceProperty_SoundBankFSRef,
					kAudioUnitScope_Global, 
					0,
					&fsRef, 
					sizeof(fsRef)), 
				home);
	AUGraphStart(graph);
	initChannelInstrument();
	
home:
	return result == noErr;
}


/*****************************************************************************
	sqSoftSynthRestoreDLS
	
	use DLSSynth
	@param none
	@return noErr or not
	@TODO i can NOT AUGraphUpdate
******************************************************************************/
int sqSoftSynthRestoreDLS(void) {
	OSStatus result;
	AudioComponentDescription cd;
	//AudioUnit synthUnit;
	UInt32 usesReverb;
	int i;
	
	if (false) {
	
	AUGraphStop(graph);
	AUGraphRemoveNode(graph, synthNode);

	// Open the DLS Synth
	cd.componentType = kAudioUnitType_MusicDevice;
	cd.componentSubType = kAudioUnitSubType_DLSSynth;
	cd.componentManufacturer = kAudioUnitManufacturer_Apple;
	cd.componentFlags = 0;
	cd.componentFlagsMask = 0;
	require_noerr(result = AUGraphAddNode(graph, &cd, &synthNode), home);
    
	initChannelInstrument();

	// Connect the devices up
	require_noerr(result = AUGraphConnectNodeInput(graph, synthNode, 1, delayNode, 0), home);
	require_noerr(result = AUGraphConnectNodeInput(graph, delayNode, 0, reverbNode, 0), home);
	require_noerr(result = AUGraphConnectNodeInput(graph, reverbNode, 0, outNode, 0), home);
	require_noerr(result = AUGraphUpdate(graph, NULL), home);
	AUGraphUpdate (graph, NULL);

	// Turn off the reverb on the synth
	require_noerr(result = AUGraphNodeInfo(graph, synthNode, 0, &synthUnit), home);
	usesReverb = 0;
	require_noerr(result = AudioUnitSetProperty(
					synthUnit,
					kMusicDeviceProperty_UsesInternalReverb, 
					kAudioUnitScope_Global,
					0,
					&usesReverb, sizeof(usesReverb)
	), home);
		
	}
	
	midiShutdown();
	midiInit();
	AUGraphStart(graph);
	
home:
	return result == noErr;
}


/*------------------------------------------------------------------------------*/
/*							AudioUnit											*/
/*------------------------------------------------------------------------------*/

/*****************************************************************************
	sqSoftSynthGetAudioUnitParameter
	
	answer component name and ParameterInfo of given unit by index.
	@param index
	@return -1
	@TODO require implementation
******************************************************************************/
int sqSoftSynthGetAudioUnitParameter(int index) {
	//self flag: #TODO;
	return -1;
}


/*----------------------------------------------------------------------------
	getAudioUnitParameter
	
	print component name and ParameterInfo of given unit.
	@param an AudioUnit
	@return void
	@TODO require rewriting to answer real getAudioUnitParameter to answer not to print
-----------------------------------------------------------------------------*/
static void getAudioUnitParameter(AudioUnit unit) {
	UInt32 paramSize;
	int numberOfParams;
	
	AudioUnitGetPropertyInfo(unit,
				kAudioUnitProperty_ParameterList,
				kAudioUnitScope_Global,
				0,
				&paramSize,
				NULL);
	numberOfParams = paramSize / sizeof(AudioUnitParameterID);
	AudioUnitParameterID paramList[numberOfParams];
	AudioUnitGetProperty(unit,
				kAudioUnitProperty_ParameterList,
				kAudioUnitScope_Global,
				0,
				paramList,
				&paramSize);
	int i;
	CFStringRef nameRef;
	AudioComponent component = AudioComponentInstanceGetComponent(unit);
	getBusNumber(component);
	AudioComponentCopyName(component, &nameRef);
	const char *componentName = CFStringGetCStringPtr(nameRef, encoding);
	for (i = 0; i < numberOfParams; i++) {
		int parameterID = paramList[i];
		AudioUnitParameterInfo paramInfo;
		OSStatus result;
		
		UInt32 paramInfoSize = sizeof(paramInfo);
		result = AudioUnitGetProperty(unit,
					kAudioUnitProperty_ParameterInfo,
					kAudioUnitScope_Global,
					parameterID,
					&paramInfo,
					&paramInfoSize);
		if (result != noErr) continue;
//		CFStringRef nameRef;
//		AudioComponentCopyName(AudioComponentInstanceGetComponent(unit), &nameRef);
//		const char *componentName = CFStringCopyUTF8String(nameRef);
		const char *unitName = "---";
		UInt32 clumpID = 0;
		const char *cfNameString;
		if (paramInfo.flags & kAudioUnitParameterFlag_HasName) {
			unitName = CFStringGetCStringPtr(paramInfo.unitName, encoding);
		}
		if (paramInfo.flags & kAudioUnitParameterFlag_HasClump) {
			clumpID = paramInfo.clumpID;
		}
		if (paramInfo.flags & kAudioUnitParameterFlag_HasCFNameString) {
			cfNameString = CFStringGetCStringPtr(paramInfo.cfNameString, encoding);
		}
		printf("%s: %s %s %ld %s %f %f %f\n", 
					componentName,
					paramInfo.name, 
					unitName,
					clumpID,
					cfNameString,
					paramInfo.minValue,
					paramInfo.maxValue,
					paramInfo.defaultValue);
		if (paramInfo.flags & kAudioUnitParameterFlag_CFNameRelease) {
			if (paramInfo.flags & kAudioUnitParameterFlag_HasCFNameString && paramInfo.cfNameString != NULL) {
				CFRelease(paramInfo.cfNameString);
			}
			if (paramInfo.unit == kAudioUnitParameterUnit_CustomUnit && paramInfo.unitName != NULL) {
				CFRelease(paramInfo.unitName);
			}
		}
	}
	CFRelease(nameRef);
}

/*----------------------------------------------------------------------------
	getAudioUnitParameterFromNode
	
	print component name and ParameterInfo of given anAUNode.
	@param an AUNode
	@return void
	@TODO require rewriting to answer real getAudioUnitParameter to answer not to print
-----------------------------------------------------------------------------*/
static void getAudioUnitParameterFromNode(AUNode node) {
	AudioUnit unit;
	
	AUGraphNodeInfo(graph, node, 0, &unit);
	return getAudioUnitParameter(unit);
}


/*****************************************************************************
	sqSoftSynthGetUsedAudioUnitCount
	
	answer the number of components currently using nodes.
	@param none
	@return the number of components currently using nodes.
******************************************************************************/
int sqSoftSynthGetUsedAudioUnitCount(void) {
	OSStatus result;
	UInt32 outNumberOfNodes;
	
	require_noerr(result = AUGraphGetNodeCount(graph, &outNumberOfNodes), home);
	return outNumberOfNodes;
	
home:
	return 0;
}


/*****************************************************************************
	sqSoftSynthGetAudioUnitCount
	
	answer the number of components system installed.
	@param none
	@return the number of components system installed.
******************************************************************************/
int sqSoftSynthGetAudioUnitCount(void) {
	AudioComponentDescription cd;
	
	cd.componentType = 0;
	cd.componentSubType = 0;
	cd.componentManufacturer = 0;
	cd.componentFlags = 0;
	cd.componentFlagsMask = 0;
	return AudioComponentCount(&cd);
	
home:
	return 0;
}


/*****************************************************************************
	sqSoftSynthGetAudioComponentDescription
	
	answer the anAudioComponentDescription at given index.
	@param index
	@return the oop of anAudioComponentDescription at given index.
******************************************************************************/
sqInt sqSoftSynthGetAudioComponentDescriptioninto(int index, sqInt oop) {
	AudioComponentDescription cd;
	AudioComponent component = NULL;
	int maxConponents = sqSoftSynthGetAudioUnitCount();
	int count = 0;
	
	cd.componentType = 0;
	cd.componentSubType = 0;
	cd.componentManufacturer = 0;
	cd.componentFlags = 0;
	cd.componentFlagsMask = 0;
	while (component = AudioComponentFindNext(component, &cd)) {
		if (index == count) {
			return getAudioComponentDescription(component, oop);
		}
		count++;
	}
	return oop;
}
	

/*----------------------------------------------------------------------------
	getAudioUnitName
	
	answer the name of an AudioUnit.
	@param an AudioUnit
	@param an AudioComponent
-----------------------------------------------------------------------------*/
static CFStringRef getAudioUnitName(AudioUnit unit) {
	return getAudioComponentName(AudioComponentInstanceGetComponent(unit));
}


/*----------------------------------------------------------------------------
	getAudioComponentName
	
	answer the name of an AudioComponent.
	@param an AudioComponent
	@return the name of an AudioComponent. 
-----------------------------------------------------------------------------*/
static CFStringRef getAudioComponentName(AudioComponent component) {
	OSStatus result;
	CFStringRef nameRef;
	
	require_noerr(result = AudioComponentCopyName(component, &nameRef), end);
	if (nameRef == NULL || 
			(CFStringCompare(nameRef, CFSTR(""), 0) == kCFCompareEqualTo)) {
		nameRef = CFSTR("<unknown>");
	}
	return nameRef;

end:
	return CFSTR("<unknown>");
}


/*----------------------------------------------------------------------------
	getAudioComponentDescription
	
	answer the anAudioComponentDescription at given index.
	@param an AudioComponent
	@param an Smalltlak instance of AudioComponentDescription 
	@return an Smalltlak instance of AudioComponentDescription 
-----------------------------------------------------------------------------*/
static sqInt getAudioComponentDescription(AudioComponent component, sqInt rcvr) {
	OSStatus result;
	CFStringRef nameRef;
	AudioComponentDescription cd;
	sqInt oop;
	UInt32 version = 0L;
	const char *name = NULL;

	nameRef = getAudioComponentName(component);
	require_noerr(result = AudioComponentGetDescription(component, &cd), end);
	if (AudioComponentGetVersion(component, &version) != noErr) {
		// UInt32 -> UInt32 -> unsigned long
		version = 0L;
	}
	#ifdef __DEBUG__
	OSType type;
	OSType subtype;
	OSType manufacturer;
	type = CFSwapInt32HostToBig(cd.componentType);
	subtype = CFSwapInt32HostToBig(cd.componentSubType);
	manufacturer = CFSwapInt32HostToBig(cd.componentManufacturer);
	printf("%s\t'%4.4s'\t'%4.4s'\t'%4.4s'\t%ld.%ld.%ld\n", 
				CFStringGetCStringPtr(nameRef, encoding), 
				(char *) &type,
				(char *) &subtype,
				(char *) &manufacturer,
				version >> 16, (version >> 8) & 0xff, version & 0xff);
	#endif //__DEBUG__
	// set to AudioComponentDescription class
	/* instanceVariableNames: 
		0: componentName 
		1: componentType 
		2: componentSubType 
		3: componentManufacturer 
		4: version 
	 */
	/* AudioComponent.h
		typedef struct AudioComponentDescription {
		   OSType  componentType;
		   OSType  componentSubType;
		   OSType  componentManufacturer;
		   UInt32  componentFlags;
		   UInt32  componentFlagsMask;
		} AudioComponentDescription;
	 */
	// componentName (CFStringRef)
	name = CFStringGetCStringPtr(nameRef, encoding);
	oop = stringToOop(name);
	interpreterProxy->success(oop != null);
	if (interpreterProxy->failed()) {
		return null;
	}
	interpreterProxy->storePointerofObjectwithValue(0, rcvr, oop);
	// componentType (OSType -> FourCharCode -> UInt32 -> unsigned long)
	oop = interpreterProxy->positive32BitIntegerFor(cd.componentType);
	interpreterProxy->success(oop != null);
	if (interpreterProxy->failed()) {
		return null;
	}
	interpreterProxy->storePointerofObjectwithValue(1, rcvr, oop);
	// componentSubType (OSType -> FourCharCode -> UInt32 -> unsigned long)
	oop = interpreterProxy->positive32BitIntegerFor(cd.componentSubType);
	interpreterProxy->success(oop != null);
	if (interpreterProxy->failed()) {
		return null;
	}
	interpreterProxy->storePointerofObjectwithValue(2, rcvr, oop);
	// componentManufacturer (OSType -> FourCharCode -> UInt32 -> unsigned long)
	oop = interpreterProxy->positive32BitIntegerFor(cd.componentManufacturer);
	interpreterProxy->success(oop != null);
	if (interpreterProxy->failed()) {
		return null;
	}
	interpreterProxy->storePointerofObjectwithValue(3, rcvr, oop);
	// version (UInt32 -> unsigned long)
	oop = interpreterProxy->positive32BitIntegerFor(version);
	interpreterProxy->success(oop != null);
	if (interpreterProxy->failed()) {
		return null;
	}
	interpreterProxy->storePointerofObjectwithValue(4, rcvr, oop);

end:
	CFRelease(nameRef);
	return rcvr;
}


/*!
	AUComponent.h
	
    @enum           Audio Unit Types
    @abstract		different types of audio units
	@discussion		Audio unit's are classified into different types, where those types perform different roles and functions.
					There are some general categories of functionality that apply across different types of audio units:
					(1) Real-time usage
						The audio unit will complete its operations in less time that is represented by the render buffer.
						All audio units with the exception of the OfflineEffect should meet this criteria
					(2) Real-time I/O
						Will request the same amount of audio input as it is being asked to produce for output. Effects, Panners, 
						Mixers and MusicDevices are required to adhere to this restriction. FormatConverter's can with some contraints
						be used in this situation (for instance, sample rate conversion, float-int), but the host of the
						audio unit is responsible for insuring this.
					(3) UI versus Programmatic usage
						UI usage covers the case of using an audio unit in a Digital Audio Workstation (DAW) with appropriate
						UI (for example a filter in Garage Band or Logic). Effects, Panners, MusicDevices are all expected to be 
						usable within this context. 
						Programmatic usage is where an audio unit is used by a host app as part of a general signal processing chain.
						For instance, a mixer audio unit can be used to take several different audio sources in a game and mix them together.
						Mixers, Output units are for programmatic usage. 
						FormatConverter and Generator types are generally programmatic audio units, but if they can be used in a UI situation,
						they specify a custom view. The app can then use that to decide that, with appropraite constraints, the audio unit
						could be presented in a DAW type application. For instance, the AUConveter audio unit from apple can do sample rate
						conversion, etc, but has not general utility for a user in a DAW app. Apple's Varispeed or AUTimePitch audio units 
						can be used to change the playback rate and pitch and so could be used to good affect by a user in a DAW type environment,
						as well as just providing this general functionality to any program.
					
	@constant		kAudioUnitType_Output
					An output unit can be used as the head of an AUGraph. Apple provides a number of output units that interface
					directly with an audio device
					
	@constant		kAudioUnitType_MusicDevice
					Used to describe software musical instruments such as samplers and synthesisers. They respond to MIDI and create
					notes, which are then controlled through parameters or MIDI control messages. See <AudioUnit/MusicDevice.h>
										
	@constant		kAudioUnitType_MusicEffect
					Is an effect that is also able to respond directly to MIDI control messages, typically through the mapping of
					these MIDI messages to different parameters of the effect's DSP algorithm.
					
	@constant		kAudioUnitType_FormatConverter
					A format converter is a general category for audio units that can change the format (for instance, sample rate conversion)
					from an input to an output, as well as other, non-I/O type manipulations (like a deferred render or varispeed type of operation).
					As such, a format converter can ask for as much or as little audio input to produce a given output. They are still expected to
					complete their rendering within the time represented by the output buffer. 
					For format converters that have some utility as an "audio effect or processor", it is quite common to provide an offline version
					of this audio unit as well. For instance, Apple ships a format converter (for use in a "real-time" like situation) and an offline
					version (for processing audio files) of the Time Pitch and Varispeed audio units.
					
	@constant		kAudioUnitType_Effect
					An audio unit that will process some x number of audio input samples to produce x number of audio output samples. The common case for an 
					effect is to have a single input to a single output, though some effects take side-chain inputs as well. Effects can be run in
					"offline" contexts (such as procesing a file), but they are expected to run in real-time. A delay unit or reverb is
					a good example of this.
					
	@constant		kAudioUnitType_Mixer
					An audio unit that takes some number of inputs, mixing them to provide 1 or more audio outputs. A stere mixer (mono and stereo inputs
					to produce one stereo output) is an example of this.
					
	@constant		kAudioUnitType_Panner
					A panner is a specialised effect that will pan a single audio input to a single output. Panner units are required to support
					a collection of standardised parameters that specify the panning coordinates (aside from whatever custom parameters the
					panner may provide). A surround panner is an example of this
					
	@constant		kAudioUnitType_Generator
					A generator will have no audio input, but will just produce audio output. In some ways it is similar to a MusicDevice, except that a
					generator provides no MIDI input, or notion of "notes". A tone generator is a good example of this.
					
	@constant		kAudioUnitType_OfflineEffect
					An offline effect is used to process data from a file and is also used to publish a capability that cannot be run in real-time. For instance,
					the process of normalisation requires seeing the entire audio input before the scalar to apply in the normalisation process can be estimated.
					As such, offline effects also have a notion of a priming stage that can be performed before the actual rendering/processing phase is executed.
*/
/*
		kAudioUnitType_Output					= 'auou',
		kAudioUnitType_MusicDevice				= 'aumu',
		kAudioUnitType_MusicEffect				= 'aumf',
		kAudioUnitType_FormatConverter			= 'aufc',	
		kAudioUnitType_Effect					= 'aufx',	
		kAudioUnitType_Mixer					= 'aumx',
		kAudioUnitType_Panner					= 'aupn',
		kAudioUnitType_OfflineEffect			= 'auol',
		kAudioUnitType_Generator				= 'augn',
*/
/*----------------------------------------------------------------------------
	isAudioUnit
	
	answer audiounit or not given the anAudioComponentDescription.
	anAudioComponentDescription.componentType is match one of above or not.
	@param an anAudioComponentDescription
	@return audiounit or not
-----------------------------------------------------------------------------*/
static int isAudioUnit(AudioComponentDescription cd) {
	OSType componentType = cd.componentType;
	
	return (componentType == kAudioUnitType_Output || 
				componentType == kAudioUnitType_MusicDevice || 
				componentType == kAudioUnitType_MusicEffect || 
				componentType == kAudioUnitType_FormatConverter || 
				componentType == kAudioUnitType_Effect || 
				componentType == kAudioUnitType_Mixer || 
				componentType == kAudioUnitType_Panner || 
				componentType == kAudioUnitType_OfflineEffect || 
				componentType == kAudioUnitType_Generator);
}


/*****************************************************************************
	sqSoftSynthGetAudioUnitsubTypemanufacturerinto
	
	answer the audiounit from given anAudioComponentDescription parameters.
	@param componentType of anAudioComponentDescription
	@param componentSubType of anAudioComponentDescription
	@param componentManufacturer of anAudioComponentDescription
	@param oop of audiounit
	@return oop of audiounit
******************************************************************************/
int sqSoftSynthGetAudioUnitsubTypemanufacturerinto(unsigned long componentType, unsigned long componentSubType, unsigned long componentManufacturer, sqInt rcvr) {
	printf("%ld %ld %ld\n", componentType, componentSubType, componentManufacturer);
	AudioComponentDescription cd;
	OSStatus result;
	AUNode unitNode;
	AudioUnit audioUnit;

	cd.componentType = componentType;
	cd.componentSubType = componentSubType;
	cd.componentManufacturer = componentManufacturer;
	cd.componentFlags = 0;
	cd.componentFlagsMask = 0;
	if (!isAudioUnit(cd)) {
		return interpreterProxy->nilObject();
	}
	//require_noerr(result = AUGraphAddNode(graph, &cd, &unitNode), end);
	//require_noerr(result = AUGraphNodeInfo(graph, unitNode, NULL, &audioUnit), nodeInfoErr);
 	if (getAudioUnit(audioUnit, rcvr) == NULL) {
		return interpreterProxy->nilObject();
	}
	puts("now AUGraphRemoveNode");
	//require_noerr(result = AUGraphRemoveNode(graph, unitNode), nodeRemoveErr);
	return rcvr;

nodeInfoErr:
	fprintf(stderr, "can't find node information AudioUnit: componentType=%ld componentSubType=%ld componentManufacturer=%ld\n",
			componentType, componentSubType, componentManufacturer);
	return interpreterProxy->nilObject();
nodeRemoveErr:
	fprintf(stderr, "can't remove AudioUnit: componentType=%ld componentSubType=%ld componentManufacturer=%ld\n",
			componentType, componentSubType, componentManufacturer);
	return interpreterProxy->nilObject();
end:
	fprintf(stderr, "can't find AudioUnit: componentType=%ld componentSubType=%ld componentManufacturer=%ld\n",
			componentType, componentSubType, componentManufacturer);
	return interpreterProxy->nilObject();
}


/*----------------------------------------------------------------------------
	getAudioUnit
	
	get some properties of AudioUnit to oop of audiounit
	@param an AudioUnit
	@param oop of audiounit
	@return oop of audiounit
-----------------------------------------------------------------------------*/
static AudioUnit getAudioUnit(AudioUnit unit, sqInt rcvr) {
 	OSStatus result;
 	UInt32 inputBusSize, outputBusSize;
 	UInt32 size = sizeof(UInt32);
 	sqInt oop;
 
 	require_noerr(result = AudioUnitGetProperty(unit,
 							kAudioUnitProperty_ElementCount,
 							kAudioUnitScope_Input,
 							0,
 							&inputBusSize,
 							&size), end);
 	require_noerr(result = AudioUnitGetProperty(unit,
 							kAudioUnitProperty_ElementCount,
 							kAudioUnitScope_Output,
 							0,
 							&outputBusSize,
 							&size), end);
 	//printf("input[%ld] output[%ld]\n", inputBusSize, outputBusSize);
 	/* instVarAt: 0 is 'description' defined AudioComponent (superclass). */
	// store inputBusSize
	oop = interpreterProxy->integerObjectOf(inputBusSize);
	interpreterProxy->success(oop != null);
	if (interpreterProxy->failed()) {
		return null;
	}
	interpreterProxy->storePointerofObjectwithValue(1, rcvr, oop);
	// store outputBusSize
	oop = interpreterProxy->integerObjectOf(outputBusSize);
	interpreterProxy->success(oop != null);
	if (interpreterProxy->failed()) {
		return null;
	}
	interpreterProxy->storePointerofObjectwithValue(2, rcvr, oop);
 	return unit;
 
end:
 	return NULL;
}


/*------------------------------------------------------------------------------*/
/*							AUGraph												*/
/*------------------------------------------------------------------------------*/

/*****************************************************************************
	sqSoftSynthGetAUGraphNumberOfInteractions
	
	answer the number of interactions for an audio processing graph.
	@param none
	@return the number of interactions for an audio processing graph.
******************************************************************************/
int sqSoftSynthGetAUGraphNumberOfInteractions(void) {
	UInt32 interactions;
	OSStatus err;
	
	err = AUGraphGetNumberOfInteractions(graph, &interactions);
	if (err != noErr) {
		interactions = 0;
	}
	return interactions;
}


/*****************************************************************************
	sqSoftSynthGetAUGraphInteractionInfointo
	
	answer the information about a particular interaction in an audio processing graph.
	@param index of graph interactions
	@param oop of AUNodeInteraction
	@return oop of AUNodeInteraction.
******************************************************************************/
int sqSoftSynthGetAUGraphInteractionInfointo(int index, sqInt rcvr) {
	OSStatus err;
	AUNodeInteraction interaction;
	sqInt oop;
	
	err = AUGraphGetInteractionInfo(graph, index, &interaction);
	if (err != noErr) {
		return interpreterProxy->nilObject();
	}
	// nodeInteractionType (UInt32)
	oop = interpreterProxy->positive32BitIntegerFor(interaction.nodeInteractionType);
	interpreterProxy->success(oop != null);
	if (interpreterProxy->failed()) {
		return null;
	}
	interpreterProxy->storePointerofObjectwithValue(0, rcvr, oop);
	/*** callback Interaction will be set nodeInteractionType only. ***/
	if (interaction.nodeInteractionType == kAUNodeInteraction_InputCallback) {
		return rcvr;
	}
	// sourceNode (AUNode -> SInt32 -> signed long)
	oop = interpreterProxy->signed32BitIntegerFor(interaction.nodeInteraction.connection.sourceNode);
	interpreterProxy->success(oop != null);
	if (interpreterProxy->failed()) {
		return null;
	}
	interpreterProxy->storePointerofObjectwithValue(1, rcvr, oop);
	// sourceOutputNumber (UInt32 -> unsigned long)
	oop = interpreterProxy->positive32BitIntegerFor(interaction.nodeInteraction.connection.sourceOutputNumber);
	interpreterProxy->success(oop != null);
	if (interpreterProxy->failed()) {
		return null;
	}
	interpreterProxy->storePointerofObjectwithValue(2, rcvr, oop);
	// destNode (AUNode -> SInt32 -> signed long)
	oop = interpreterProxy->signed32BitIntegerFor(interaction.nodeInteraction.connection.destNode);
	interpreterProxy->success(oop != null);
	if (interpreterProxy->failed()) {
		return null;
	}
	interpreterProxy->storePointerofObjectwithValue(3, rcvr, oop);
	// UInt32 (UInt32 -> unsigned long)
	oop = interpreterProxy->positive32BitIntegerFor(interaction.nodeInteraction.connection.destInputNumber);
	interpreterProxy->success(oop != null);
	if (interpreterProxy->failed()) {
		return null;
	}
	interpreterProxy->storePointerofObjectwithValue(4, rcvr, oop);
// 	printf("type=%ld src=%ld srcO=%ld dst=%ld dstI=%ld\n", interaction.nodeInteractionType,
// 								interaction.nodeInteraction.connection.sourceNode,
// 								interaction.nodeInteraction.connection.sourceOutputNumber,
// 								interaction.nodeInteraction.connection.destNode,
// 								interaction.nodeInteraction.connection.destInputNumber);
	return rcvr;
}


/*----------------------------------------------------------------------------
	getAGetAudioUnitFromAUNode
	
	answer the AudioUnit by AUNode at audio processing graph
	@param an AUNode
	@return audiounit
-----------------------------------------------------------------------------*/
static AudioUnit getAGetAudioUnitFromAUNode(AUNode node) {
	OSStatus err;
	AudioUnit unit;
	AudioComponentDescription cd;

	err = AUGraphNodeInfo(graph, node, &cd, &unit);
	if (err != noErr) {
		return NULL;
	}
	return unit;
}


/*****************************************************************************
	sqSoftSynthEditAudioUnitFromAUNode
	
	open native view of the AudioUnit by AUNode at audio processing graph.
	@param anAUNode
	@return success or not.
******************************************************************************/
int sqSoftSynthEditAudioUnitFromAUNode(signed long theNode) {
	AUNode node = theNode;
	AudioUnit unit = getAGetAudioUnitFromAUNode(node);
	if (unit == NULL) {
		return false;
	}
	// open native edit view
	id controller = [NativeUIController editorForAudioUnit: unit forceGeneric: NO delegate: nil];
	CFStringRef name = getAudioComponentName(AudioComponentInstanceGetComponent(unit));
	[[controller window] setTitle: (NSString *) name];
	return true;
}


/*****************************************************************************
	sqSoftSynthGetAudioUnitFromAUNodeintodescription
	
	answer the AudioUnit by AUNode at audio processing graph.
	@param anAUNode
	@param oop of AudioUnit
	@param oop of AudioComponentDescription
	@return oop of AudioUnit.
******************************************************************************/
int sqSoftSynthGetAudioUnitFromAUNodeintodescription(signed long node, sqInt audioUnitOop, sqInt audioComponentDescriptionOop) {
	AudioUnit unit;
	
	unit = getAGetAudioUnitFromAUNode(node);
	if (unit == NULL) {
		return interpreterProxy->nilObject();
	}
	AudioComponent component = AudioComponentInstanceGetComponent(unit);
	getAudioComponentDescription(component, audioComponentDescriptionOop);
	// store description
	interpreterProxy->storePointerofObjectwithValue(0, audioUnitOop, audioComponentDescriptionOop);
	getAudioUnit(unit, audioUnitOop);
	return audioUnitOop;
}


/*----------------------------------------------------------------------------
	audioComponentDescriptionFromOop
	
	answer anAudioComponentDescription from oop of new AudioComponent.
	@param oop of new AudioComponent
	@return anAudioComponentDescription
-----------------------------------------------------------------------------*/
static AudioComponentDescription audioComponentDescriptionFromOop(sqInt descriptionOop) {
	//instVars of AudioComponentDescription: 
	//	0: componentName 
	//	1: componentType 
	//	2: componentSubType 
	//	3: componentManufacturer 
	//	4: version
	AudioComponentDescription cd;
	sqInt componentTypeOop, componentSubTypeOop, componentManufacturerOop;
	
	// get new audiounit
	componentTypeOop = interpreterProxy->fetchPointerofObject(1, descriptionOop);
	componentSubTypeOop = interpreterProxy->fetchPointerofObject(2, descriptionOop);
	componentManufacturerOop = interpreterProxy->fetchPointerofObject(3, descriptionOop);
	cd.componentType = interpreterProxy->positive64BitValueOf(componentTypeOop);
	cd.componentSubType = interpreterProxy->positive64BitValueOf(componentSubTypeOop);
	cd.componentManufacturer = interpreterProxy->positive64BitValueOf(componentManufacturerOop);
	cd.componentFlags = 0;
	cd.componentFlagsMask = 0;
	return cd;
}


/*****************************************************************************
	sqSoftSynthReplaceAUNodewith
	
	replace node with new audio component at audio processing graph.
	@param an old AUNode
	@param oop of new AudioComponent
	@return success or not.
******************************************************************************/
int sqSoftSynthReplaceAUNodewith(signed long oldNode, sqInt newComponentDescriptionOop) {
	AudioComponentDescription cd = audioComponentDescriptionFromOop(newComponentDescriptionOop);
	NSLog(@"sqSoftSynthReplaceAUNodewith %d[%@] %d[%@] %d[%@]", 
						cd.componentType,
						CreateTypeStringWithOSType(cd.componentType), 
						cd.componentSubType,
						CreateTypeStringWithOSType(cd.componentSubType), 
						cd.componentManufacturer,
						CreateTypeStringWithOSType(cd.componentManufacturer));
	return replaceNodeInAUGraph(graph, oldNode, cd) == noErr;
}


/*----------------------------------------------------------------------------
	replaceNodeInAUGraph
	
	replace node with new node at audio processing graph.
	new node will be constructed by given an AudioComponentDescription.
	@param graph
	@param oldNode
	@param newAudioComponentDescription
	@return success or not
-----------------------------------------------------------------------------*/
static OSStatus replaceNodeInAUGraph(AUGraph graph, AUNode oldNode, AudioComponentDescription newAudioComponentDescription) {
 	OSStatus err;
	UInt32 numberOfAllInteractions;
	UInt32 numberOfInteractionsToOldNode = 0;
	UInt32 numberOfInteractionsFromOldNode = 0;
	AUNodeInteraction *interactionsToOldNode = NULL;
	AUNodeInteraction *interactionsFromOldNode = NULL;
	
	// stop the augrpah
	//require_noerr(err = AUGraphStop(graph), err);
	//AUGraphUninitialize(graph);
	
	// find all interaction with old node. sourceNode and destNode, both.
	require_noerr(err = AUGraphGetNumberOfInteractions(graph, &numberOfAllInteractions), err);
	interactionsToOldNode = malloc(sizeof(AUNodeInteraction) * numberOfAllInteractions);
	interactionsFromOldNode = malloc(sizeof(AUNodeInteraction) * numberOfAllInteractions);
	
	for (int i = 0; i < numberOfAllInteractions; i++) {
		AUNodeInteraction interaction;
		require_noerr(err = AUGraphGetInteractionInfo(graph, i, &interaction), err);
		if (interaction.nodeInteractionType == kAUNodeInteraction_InputCallback) {
			continue;
		}
		if (oldNode == interaction.nodeInteraction.connection.destNode) {
			// oldNode == destNode
			interactionsToOldNode[numberOfInteractionsToOldNode++] = interaction;
		} else if (oldNode == interaction.nodeInteraction.connection.sourceNode) {
			// oldNode == sourceNode
			interactionsFromOldNode[numberOfInteractionsFromOldNode++] = interaction;
		}
	}
	// remove old node
	require_noerr(err = AUGraphRemoveNode(graph, oldNode), err);
	// add new node
	AUNode newNode;
	require_noerr(err = AUGraphAddNode(graph, &newAudioComponentDescription, &newNode), err);
	// reconnect oldnode (destNode)
	for (int i = 0; i < numberOfInteractionsToOldNode; i++) {
		AUNodeConnection connection = interactionsToOldNode[i].nodeInteraction.connection;
		require_noerr(err = AUGraphConnectNodeInput(graph, 
													connection.sourceNode,
													connection.sourceOutputNumber,
													newNode,
													connection.destInputNumber), err);
	}
	// reconnect oldnode (sourceNode)
	for (int i = 0; i < numberOfInteractionsToOldNode; i++) {
		AUNodeConnection connection = interactionsFromOldNode[i].nodeInteraction.connection;
		require_noerr(err = AUGraphConnectNodeInput(graph, 
													newNode,
													connection.sourceOutputNumber,
													connection.destNode,
													connection.destInputNumber), err);
	}
	// start the augrpah
	//require_noerr(err = AUGraphStart(graph), err);
	// update grpah
	require_noerr(err = AUGraphUpdate(graph, NULL), err);
	//AUGraphInitialize(graph);

	// start the augrpah
	//require_noerr(err = AUGraphStart(graph), err);

err:
	if (interactionsToOldNode) {
		free(interactionsToOldNode);
	}
	if (interactionsFromOldNode) {
		free(interactionsFromOldNode);
	}
	if (err != noErr) {
		NSLog(@"replaceNodeInAUGraph: err: %d", err);
	}
 	return err;
}


/*****************************************************************************
	sqSoftSynthRemoveAUNode
	
	remove node at audio processing graph.
	@param node
	@return success or not
******************************************************************************/
int sqSoftSynthRemoveAUNode(AUNode node) {
	return removeAUNode(graph, node) == noErr;
}


/*----------------------------------------------------------------------------
	removeAUNode
	
	remove node at audio processing graph.
	@param graph
	@param node
	@return success or not
-----------------------------------------------------------------------------*/
static OSStatus removeAUNode(AUGraph graph, AUNode node) {
	OSStatus err;
	UInt32 ioNumInteractions = 0;
	Boolean outIsRunning;
	AUNodeInteraction *interactions = NULL;
	
	require_noerr(err = AUGraphIsRunning(graph, &outIsRunning), err);
	// get the number of interactions
	require_noerr(err = AUGraphCountNodeInteractions(graph, node, &ioNumInteractions), err);
	interactions = malloc(sizeof(AUNodeInteraction) * ioNumInteractions);
	// get the interactions
	require_noerr(err = AUGraphGetNodeInteractions(graph, node, &ioNumInteractions, interactions), err);
	// enumerate the interaction to get the nodes before and after the node for re-connection.
	AUNode previousNode = LONG_MIN;
	AUNode nextNode = LONG_MIN;
	UInt32 previousOutputNumber;
	UInt32 nextInputNumber;
	for (int i = 0; i < ioNumInteractions; i++) {
		AUNodeInteraction interaction = interactions[i];
		if (interaction.nodeInteractionType != kAUNodeInteraction_Connection) {
			continue;
		}
		AudioUnitNodeConnection connection = interaction.nodeInteraction.connection;
		if (node == connection.destNode) {
			previousNode = connection.sourceNode;
			previousOutputNumber = connection.sourceOutputNumber;
			//NSLog(@"previousNode=%d previousOutputNumber=%d", previousNode, previousOutputNumber);
		}
		if (node == connection.sourceNode) {
			nextNode = connection.destNode;
			nextInputNumber = connection.destInputNumber;
			//NSLog(@"nextNode=%d nextInputNumber=%d", nextNode, nextInputNumber);
		}
	}
	// disconnect existing node input connections
	require_noerr(err = AUGraphDisconnectNodeInput(graph, node, previousOutputNumber), err);
	require_noerr(err = AUGraphDisconnectNodeInput(graph, nextNode, nextInputNumber), err);
	// remove node
	require_noerr(err = AUGraphRemoveNode(graph, node), err);
/*
enum
{
	kAUGraphErr_NodeNotFound 				= -10860,
	kAUGraphErr_InvalidConnection 			= -10861,
	kAUGraphErr_OutputNodeErr				= -10862,
	kAUGraphErr_CannotDoInCurrentContext	= -10863,
	kAUGraphErr_InvalidAudioUnit			= -10864
};
*/
	// re-connection
	if ((previousNode != LONG_MIN) && (nextNode != LONG_MIN)) {
		require_noerr(err = AUGraphConnectNodeInput(graph, previousNode, previousOutputNumber, nextNode, nextInputNumber), err);
	}

	require_noerr(err = AUGraphUpdate(graph, NULL), err);
	if (outIsRunning) {
		AUGraphStart(graph);
	}
	
err:
	if (interactions != NULL) {
		free(interactions);
	}
	if (err != noErr) {
		NSLog(@"removeAUNode: err: %d", err);
	}
	return err;
}


/*****************************************************************************
	sqSoftSynthAddComponentafter
	
	add node with new audio component after node at audio processing graph.
	@param oop of new AudioComponent
	@param node
	@return success or not
******************************************************************************/
int sqSoftSynthAddComponentafter(sqInt newComponentDescriptionOop, signed long node) {
	OSStatus error;
	AUNode newNode;
	
	AudioComponentDescription cd = audioComponentDescriptionFromOop(newComponentDescriptionOop);
	return addComponentafter(graph, cd, node) == noErr;
}


/*----------------------------------------------------------------------------
	addComponentafter
	
	add node with new audio component after node at audio processing graph.
	@param graph
	@param newAudioComponentDescription
	@param node
	@return success or not
-----------------------------------------------------------------------------*/
static OSStatus addComponentafter(AUGraph graph, AudioComponentDescription newAudioComponentDescription, AUNode node) {
	OSStatus err;
	AUNode newNode;
	UInt32 ioNumInteractions = 0;
	AUNodeInteraction *interactions = NULL;
	Boolean graphWasInitialized;

// 	AUGraphIsInitialized(graph, &graphWasInitialized);
// 	if	(graphWasInitialized) {
// 		AUGraphUninitialize(graph);
// 		AUGraphClose(graph);
// 	}
	// create new node
	require_noerr_action(err = AUGraphAddNode(graph, &newAudioComponentDescription, &newNode), err,
			NSLog(@"addComponentafter: can't create new AUNode."));
	// get the number of interactions
	require_noerr_action(err = AUGraphCountNodeInteractions(graph, node, &ioNumInteractions), err,
			NSLog(@"addComponentafter: can't count node interactions."));
	interactions = malloc(sizeof(AUNodeInteraction) * ioNumInteractions);
	// get the interactions
	require_noerr_action(err = AUGraphGetNodeInteractions(graph, node, &ioNumInteractions, interactions), err,
			NSLog(@"addComponentafter: can't get node interactions."));
	AUNode previousNode = LONG_MIN;
	AUNode nextNode = LONG_MIN;
	UInt32 previousOutputNumber;
	UInt32 nextInputNumber;
	for (int i = 0; i < ioNumInteractions; i++) {
		AUNodeInteraction interaction = interactions[i];
		if (interaction.nodeInteractionType != kAUNodeInteraction_Connection) {
			continue;
		}
		AudioUnitNodeConnection connection = interaction.nodeInteraction.connection;
		if (node == connection.sourceNode) {
			nextNode = connection.destNode;
			nextInputNumber = connection.destInputNumber;
		}
		if (node == connection.destNode) {
			previousNode = connection.sourceNode;
			previousOutputNumber = connection.sourceOutputNumber;
		}
	}
	if (!nsAlert("now adding new node", "")) goto err;
	// disconnect existing node outout connections
	require_noerr_action(err = AUGraphDisconnectNodeInput(graph, nextNode, nextInputNumber), err,
			NSLog(@"addComponentafter: can't disconnect next AUNode."));
	// connect new node input with node
	// connect new node output with next node
	require_noerr_action(err = AUGraphConnectNodeInput(graph, newNode, 0, nextNode, nextInputNumber), err,
			NSLog(@"addComponentafter: can't connect new AUNode with next AUNode: %ld -> %ld", nextNode, newNode));
	require_noerr_action(err = AUGraphConnectNodeInput(graph, node, previousOutputNumber, newNode, 0), err,
			NSLog(@"addComponentafter: can't connect node with new AUNode: %ld -> %ld", node, newNode));
	// update graph
	require_noerr_action(err = AUGraphUpdate(graph, NULL), err,
			NSLog(@"addComponentafter: can't update graph."));
// 	AUGraphInitialize(graph);
// 	AUGraphOpen(graph);
	return err;
	
err:
	if (interactions != NULL) {
		free(interactions);
	}
	NSLog(@"addComponentafter err: %d", err);
	err = AUGraphRemoveNode(graph, newNode);
	newNode = 0;
	AUGraphUpdate(graph, NULL);
	return err;
}


static int nsAlert(char *cmessageText, char *cinformativeText) {
	if (cmessageText == NULL) cmessageText = "<none>";
	if (cinformativeText == NULL) cinformativeText = "<none>";
	NSString *messageText = [[NSString alloc] initWithUTF8String:cmessageText];
	NSString *informativeText = [[NSString alloc] initWithUTF8String:cinformativeText];
	NSAlert *alert = [NSAlert alertWithMessageText: messageText
	defaultButton: @"OK" 
	alternateButton: @"cancel" 
	otherButton: nil
	informativeTextWithFormat: informativeText ];
	// default = 1 / alternate = 0 / other = -1
	return [alert runModal];
}

/*------------------------------------------------------------------------------*/
/*							AudioDevice											*/
/*------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
	theDeviceList
	
	for audioDeviceNames of input/output.
	it should be set before execute audioDeviceNames, and clear after.
-----------------------------------------------------------------------------*/
static AudioDeviceID *theDeviceList = NULL;


/*----------------------------------------------------------------------------
	propertySizeOfAudioDevices
	
	answer the size of properties of all devices.
	@param none
	@return the size of properties of all devices
-----------------------------------------------------------------------------*/
static UInt32 propertySizeOfAudioDevices(void) {
	OSStatus result;
	UInt32 thePropSize;
	AudioObjectPropertyAddress thePropertyAddress = {
				kAudioHardwarePropertyDevices,
				kAudioObjectPropertyScopeGlobal,
				kAudioObjectPropertyElementMaster
			};
	int theNumDevices;
	// get all device list
	require_noerr(result = AudioObjectGetPropertyDataSize(kAudioObjectSystemObject,
					&thePropertyAddress,
					0,
					NULL,
					&thePropSize), 
				end);
	return thePropSize;
	
end:
	return 0;
}


/*****************************************************************************
	sqSoftSynthGetAudioDeviceCount
	
	answer the number of audio devices.
	@param none
	@return number of audio devices
******************************************************************************/
int sqSoftSynthGetAudioDeviceCount(void) {
	return propertySizeOfAudioDevices() / sizeof(AudioDeviceID);
	
end:
	return 0;
}


/*****************************************************************************
	sqSoftSynthAudioDeviceNameOfdirection
	
	answer the name of audio device given index and direction. input: 0, output: 1.
	main handler of 'GetAudioDeviceName'. do not send me individually.
	for example: 
		self preamble: direction.
	^ [ 
	((0 to: self primAudioDeviceCount)
		inject: OrderedCollection new
		into: [ :names :index | 
			names
				add: (self primAudioDeviceNameOf: index direction: direction);
				yourself ]) select: [ :each | each notNil ] ]
		ensure: [ self postscript: direction ]
	@param index of theDeviceList
	@param direction IN: 0, OUT: 1
	@return name of audio device
******************************************************************************/
const char *sqSoftSynthAudioDeviceNameOfdirection(int index, int direction) {
	UInt32 thePropSize;
	CFStringRef theDeviceName;
	AudioObjectPropertyAddress thePropertyAddress;
	OSStatus result;
	
	// set anAudioObjectPropertyAddress
	thePropSize = sizeof(CFStringRef);
	thePropertyAddress.mSelector = kAudioObjectPropertyName;
	thePropertyAddress.mScope = kAudioObjectPropertyScopeGlobal;
	thePropertyAddress.mElement = kAudioObjectPropertyElementMaster;
	
	// get the device name
	if (!theDeviceList[index]) return NULL;
	require_noerr(result = AudioObjectGetPropertyData(theDeviceList[index],
					&thePropertyAddress,
					0,
					NULL,
					&thePropSize,
					&theDeviceName), 
				end);
	#ifdef __DEBUG__
	CFShow(theDeviceName);
	#endif //__DEBUG__
	const char *deviceName = CFStringGetCStringPtr(theDeviceName, kCFStringEncodingMacRoman);
	CFRelease(theDeviceName);
	return deviceName;
	
  end:
	return NULL;
}


/*****************************************************************************
	sqSoftSynthPreambleGetAudioDeviceName
	
	preamble handler of 'GetAudioDeviceName'. do not send me individually.
	for example: 
		self preamble: direction.
	^ [ 
	((0 to: self primAudioDeviceCount)
		inject: OrderedCollection new
		into: [ :names :index | 
			names
				add: (self primAudioDeviceNameOf: index direction: direction);
				yourself ]) select: [ :each | each notNil ] ]
		ensure: [ self postscript: direction ]
	@param direction IN: 0, OUT: 1
	@return direction
******************************************************************************/
int sqSoftSynthPreambleGetAudioDeviceName(int direction) {
	sqSoftSynthPostscriptGetAudioDeviceName(direction);
	OSStatus result;
	UInt32 thePropSize;
	//AudioDeviceID *theDeviceList = NULL;
	UInt32 theNumDevices = 0;
	AudioObjectPropertyAddress thePropertyAddress = {
				kAudioHardwarePropertyDevices,
				kAudioObjectPropertyScopeGlobal,
				kAudioObjectPropertyElementMaster
	};
	
	// Find out how many devices are on the system
	thePropSize = propertySizeOfAudioDevices();
	theNumDevices = thePropSize / sizeof(AudioDeviceID);
	theDeviceList = (AudioDeviceID *) calloc(theNumDevices, sizeof(AudioDeviceID));
	require_noerr(result = AudioObjectGetPropertyData(kAudioObjectSystemObject,
					&thePropertyAddress,
					0,
					NULL,
					&thePropSize,
					theDeviceList),
				end);
	
	// get the device list for given direction
	if (direction < OUT) {
		thePropertyAddress.mScope = kAudioDevicePropertyScopeInput;
	} else {
		thePropertyAddress.mScope = kAudioDevicePropertyScopeOutput;
	}
	for (int i = 0; i < theNumDevices; i++) {
		Boolean writeable;
		thePropertyAddress.mSelector = kAudioDevicePropertyStreams;
		require_noerr(result = AudioObjectGetPropertyDataSize(theDeviceList[i], 
						&thePropertyAddress, 
						0, 
						NULL, 
						&thePropSize), 
					end);
		require_noerr(result = AudioObjectIsPropertySettable(theDeviceList[i], 
						&thePropertyAddress, 
						&writeable), 
					end);
		if (!thePropSize) theDeviceList[i] = 0;
	}
	
end:
	return direction;
}


/*****************************************************************************
	sqSoftSynthPostscriptGetAudioDeviceName
	
	postscript handler of 'GetAudioDeviceName'. do not send me individually.
	for example: 
		self preamble: direction.
	^ [ 
	((0 to: self primAudioDeviceCount)
		inject: OrderedCollection new
		into: [ :names :index | 
			names
				add: (self primAudioDeviceNameOf: index direction: direction);
				yourself ]) select: [ :each | each notNil ] ]
		ensure: [ self postscript: direction ]
	@param direction IN: 0, OUT: 1
	@return direction
******************************************************************************/
int sqSoftSynthPostscriptGetAudioDeviceName(int direction) {
	if (theDeviceList) {
		free(theDeviceList);
	}
	theDeviceList = NULL;
	return direction;
}


/*****************************************************************************
	sqSoftSynthCurrentOutputDeviceName
	
	answer audio output device name currently using.
	@param none
	@return const char *(audio output device name currently using)
******************************************************************************/
const char *sqSoftSynthCurrentOutputDeviceName(void) {
	AudioUnit unit;
	AudioDeviceID deviceID;
	UInt32 deviceIDSize = sizeof(deviceID);
	OSStatus result;
	
	require_noerr(result = AUGraphNodeInfo(graph, outNode, 0, &unit), home);
	require_noerr(result = AudioUnitGetProperty(unit,
					kAudioOutputUnitProperty_CurrentDevice,
					kAudioUnitScope_Global,
					0,
					&deviceID,
					&deviceIDSize), 
				home);
	
	CFStringRef theDeviceName = NULL;
	
	UInt32 dataSize = sizeof(theDeviceName);
	AudioObjectPropertyAddress propertyAddress = {
				kAudioDevicePropertyDeviceNameCFString,
				kAudioObjectPropertyScopeGlobal,
				kAudioObjectPropertyElementMaster
	};
	require_noerr(result = AudioObjectGetPropertyData(deviceID, 
					&propertyAddress, 
					0, 
					NULL, 
					&dataSize,
					&theDeviceName), 
				home);
	const char *deviceName = CFStringGetCStringPtr(theDeviceName, kCFStringEncodingMacRoman);
	CFRelease(theDeviceName);
	return deviceName;
	
  home:
	return NULL;
}


/*----------------------------------------------------------------------------
	setOutputDevice
	
	set audio output device to given anAudioDeviceID.
	@param deviceID
	@param AUNode kAudioUnitSubType_HALOutput outNode (currently global)
	@param AUGraph graph (currently global)
	@return success or not
-----------------------------------------------------------------------------*/
static Boolean setOutputDevice(AudioDeviceID deviceID, AUNode halNode, AUGraph theGraph) {
	AudioUnit hal;
	UInt32 deviceIDSize = sizeof(deviceID);
	OSStatus result;
	
	require_noerr(result = AUGraphNodeInfo(theGraph, halNode, 0, &hal), home);
	require_noerr(result = AudioUnitSetProperty(hal,
					kAudioOutputUnitProperty_CurrentDevice,
					kAudioUnitScope_Global,
					0,
					&deviceID,
					deviceIDSize), 
				home);
	return true;
	
home:
	return false;
}


/*****************************************************************************
	sqSoftSynthSetAudioDeviceNameddirection
	
	set audio device given name.
	@param deviceName
	@param direction
	@return success or not.
******************************************************************************/
int sqSoftSynthSetAudioDeviceNameddirection(const char * deviceName, int direction) {
	OSStatus result;
	UInt32 thePropSize;
	AudioDeviceID *theDeviceList = NULL;
	UInt32 theNumDevices = 0;
	AudioObjectPropertyAddress thePropertyAddress = {
				kAudioHardwarePropertyDevices,
				kAudioObjectPropertyScopeGlobal,
				kAudioObjectPropertyElementMaster
	};
	
	// Find out how many devices are on the system
	thePropSize = propertySizeOfAudioDevices();
	theNumDevices = thePropSize / sizeof(AudioDeviceID);
	theDeviceList = (AudioDeviceID*) calloc(theNumDevices, sizeof(AudioDeviceID));
	require_noerr(result = AudioObjectGetPropertyData(kAudioObjectSystemObject,
					&thePropertyAddress,
					0,
					NULL,
					&thePropSize,
					theDeviceList),
				end);
	
	// get the device list for given direction
	if (direction < OUT) {
		thePropertyAddress.mScope = kAudioDevicePropertyScopeInput;
	} else {
		thePropertyAddress.mScope = kAudioDevicePropertyScopeOutput;
	}
	for (int i = 0; i < theNumDevices; i++) {
		Boolean writeable;
		thePropertyAddress.mSelector = kAudioDevicePropertyStreams;
		require_noerr(result = AudioObjectGetPropertyDataSize(theDeviceList[i], 
						&thePropertyAddress, 
						0, 
						NULL, 
						&thePropSize), 
					end);
		require_noerr(result = AudioObjectIsPropertySettable(theDeviceList[i], 
						&thePropertyAddress, 
						&writeable), 
					end);
		if (!thePropSize) theDeviceList[i] = 0;
	}
	
	CFStringRef theDeviceName;
	// set anAudioObjectPropertyAddress
	thePropSize = sizeof(CFStringRef);
	thePropertyAddress.mSelector = kAudioObjectPropertyName;
	thePropertyAddress.mScope = kAudioObjectPropertyScopeGlobal;
	thePropertyAddress.mElement = kAudioObjectPropertyElementMaster;
// 	CFStringRef givenDeviceName = CFStringCreateWithCStringNoCopy(
// 				kCFAllocatorDefault,
// 				deviceName,
// 				encoding,
// 				kCFAllocatorDefault);
// Khoros(37539,0xa0884540) malloc: *** error for object 0x1b4ddcf0: pointer being freed was not allocated
// *** set a breakpoint in malloc_error_break to debug
	CFStringRef givenDeviceName = CFStringCreateWithCString(
				kCFAllocatorDefault,
				deviceName,
				encoding);
	for (int i = 0; i < theNumDevices; i++) {
		if (!theDeviceList[i]) continue;
		// get the device name
		require_noerr(result = AudioObjectGetPropertyData(theDeviceList[i],
						&thePropertyAddress,
						0,
						NULL,
						&thePropSize,
						&theDeviceName), 
					end);
#ifdef __DEBUG__
		CFShow(theDeviceName);
#endif //__DEBUG__
		
		if (CFStringCompare(theDeviceName, givenDeviceName, 0) == kCFCompareEqualTo) {
			setOutputDevice(theDeviceList[i], outNode, graph);
			break;
		}
	}
	if (theDeviceName != NULL) {
		CFRelease(theDeviceName);
	} 
	if (givenDeviceName != NULL) {
		CFRelease(givenDeviceName);
	}
	
end:
	if (theDeviceList) {
		free(theDeviceList);
	}
	 return result == noErr;
}


/********************************************************************************/
/********************************************************************************/
/*							for test/debug 										*/
/********************************************************************************/
/********************************************************************************/

/*----------------------------------------------------------------------------
	getBusNumber
	
	print component name, componentType, componentSubType, componentManufacturer, 
	input bus number and output bus number of given unit.
	@param an AudioComponent
	@return void
	@TODO require getBusNumberOfAnAudioUnit
-----------------------------------------------------------------------------*/
static void getBusNumber(AudioComponent component) {
	AudioUnit unit;
	CFStringRef nameRef;
	AudioComponentDescription ncd;
	UInt32 busCount;
	UInt32 size = sizeof(UInt32);
	
	AudioComponentCopyName(component, &nameRef);
	if (AudioComponentGetDescription(component, &ncd) == noErr) {
		OSType type;
		OSType subtype;
		OSType manufacturer;
		
		const char *name = CFStringGetCStringPtr(nameRef, encoding);
		if (name == NULL) name = "<unknown>";
		type = CFSwapInt32HostToBig(ncd.componentType);
		subtype = CFSwapInt32HostToBig(ncd.componentSubType);
		manufacturer = CFSwapInt32HostToBig(ncd.componentManufacturer);
		printf("%s\t'%4.4s'\t'%4.4s'\t'%4.4s'", 
					name, 
					(char *) &type,
					(char *) &subtype,
					(char *) &manufacturer);
		if (ncd.componentType == kAudioUnitType_Output || 
					ncd.componentType == kAudioUnitType_MusicDevice || 
					ncd.componentType == kAudioUnitType_MusicEffect || 
					ncd.componentType == kAudioUnitType_FormatConverter || 
					ncd.componentType == kAudioUnitType_Effect || 
					ncd.componentType == kAudioUnitType_Mixer || 
					ncd.componentType == kAudioUnitType_OfflineEffect || 
					ncd.componentType == kAudioUnitType_Generator) {
			if (AudioComponentInstanceNew(component, &unit) == noErr) {
				//AudioUnitInitialize(unit);
				AudioUnitGetProperty(unit,
							kAudioUnitProperty_ElementCount,
							kAudioUnitScope_Input,
							0,
							&busCount,
							&size);
				printf("\t[%ld]", busCount);
				AudioUnitGetProperty(unit,
							kAudioUnitProperty_ElementCount,
							kAudioUnitScope_Output,
							0,
							&busCount,
							&size);
				printf("\t[%ld]", busCount);
				//AudioUnitUninitialize(unit);
				AudioComponentInstanceDispose(unit);
				unit = NULL;
			}
		}
		printf("\n");
		CFRelease(nameRef);
	}
}


/*****************************************************************************
	sqSoftSynthPrintUsedAudioUnitParameter
	
	print component name and ParameterInfo of currently using nodes.
	@param none
	@return noErr or not
	@TODO require rewriting to answer real getAudioUnitParameter to answer not to print
******************************************************************************/
int sqSoftSynthPrintUsedAudioUnitParameter(void) {
	OSStatus result;
	UInt32 outNumberOfNodes;
	
	outNumberOfNodes = sqSoftSynthGetUsedAudioUnitCount();
	for (int i = 0; i < outNumberOfNodes; i++) {
		AUNode node;
		require_noerr(result = AUGraphGetIndNode(graph, i, &node), home);
		getAudioUnitParameterFromNode(node);
	}
home:
	return result == noErr;
}


#ifdef	__USE_CALLBACK__

/********************************************************************************/
/********************************************************************************/
/*							callback											*/
/********************************************************************************/
/********************************************************************************/

/*****************************************************************************
	callbackStart
	
	set callbackSemaphoreIndex.
	@param index
	@return index of semaphore.
******************************************************************************/
int callbackStart(int semaphoreIndex) {
	callbackSemaphoreIndex = semaphoreIndex;
	return callbackSemaphoreIndex;
}


/*****************************************************************************
	callbackStop
	
	clear callbackSemaphoreIndex.
	@param none
	@return index of semaphore.
******************************************************************************/
int callbackStop(void) {
	callbackSemaphoreIndex = 0;
	return callbackSemaphoreIndex;
}
#endif	//__USE_CALLBACK__
