/*
	debug.h
	SoftSynthPlugin

	Created by NISHIHARA Satoshi on 13/08/11.
	Copyright 2013 goonsh@gmail.com. All rights reserved.
 */

#define __DEBUG__

#ifdef __DEBUG__
#define	NS_BLOCK_ASSERTIONS false
#define NSLog(m, args...) NSLog(m, ##args)
#else
#define	NS_BLOCK_ASSERTIONS true
#define NSLog(m, args...)
#endif //__DEBUG__
