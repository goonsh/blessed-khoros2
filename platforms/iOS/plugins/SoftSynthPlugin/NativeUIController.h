//
//  NativeUIController.h
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/07/29.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//
//	#TODO setDelegate:
//

#import <Cocoa/Cocoa.h>
#import <AudioUnit/AudioUnit.h>
#import <AudioUnit/AudioUnitCarbonView.h>

//warning: class 'NativeUI' does not implement the 'NSWindowDelegate' protocol
@interface NativeUIController : NSWindowController <NSWindowDelegate> {

@public
	// true while processing carbon event
	BOOL isProcessingCarbonEventHandler;  

@private
	AudioUnit audioUnit;
	AudioUnitCarbonView auCarbonView;
	ComponentDescription viewComponentDescription;
	id _delegate;	
	WindowRef carbonWindowRef;
}

+ (NativeUIController *)editorForAudioUnit:(AudioUnit)unit;
+ (NativeUIController *)editorForAudioUnit:(AudioUnit)unit forceGeneric:(BOOL)forceGeneric delegate:(id)delegate;
- (id)initWithAudioUnit:(AudioUnit)unit forceGeneric:(BOOL)forceGeneric delegate:(id)delegate;
- (AudioUnit)audioUnit;

// - (void)setDelegate:(id)theDelegate;

@property BOOL isProcessingCarbonEventHandler;
@property (getter=audioUnit) AudioUnit audioUnit;
@property AudioUnitCarbonView auCarbonView;
@property (retain) id _delegate;
@property (getter=carbonWindowRef) WindowRef carbonWindowRef;
@end

@interface NSObject (NativeUIControllerProtocol)
- (void)auViewWindowWillClose: (id)auViewWindowController;
@end
