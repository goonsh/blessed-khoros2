//
//  NativeUIController.m
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/07/29.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//
//	select "AUDelay" will cause crash.
//	select "MiniSpillage" will cause crash.
//	select "AUMultibandCompressor" will cause crash.
//	comment out CFRelease(cocoaViewInfo->mCocoaAUViewBundleLocation); at getCocoaView()
//	it can show edit view for "AUDelay" and "AUMultibandCompressor"
//

#import <AudioUnit/AUCocoaUIView.h>
#import <AppKit/NSApplication.h>		// for NSAppKitVersionNumber

#import "NativeUIController.h"
#include "debug.h"

// https://developer.apple.com/library/mac/releasenotes/Cocoa/AppKit.html
// Runtime Version Check
// 
// There are several ways to check for new features provided by the Cocoa frameworks at runtime. One is to look for a given new class or method dynamically, and not use it if not there. Another is to use the global variable NSAppKitVersionNumber (or, in Foundation, NSFoundationVersionNumber):
// double NSAppKitVersionNumber;
// #define NSAppKitVersionNumber10_0 577
// ...
// #define NSAppKitVersionNumber10_6 1038
// #define NSAppKitVersionNumber10_7 1138
// One typical use of this is to floor() the value, and check against the values provided in NSApplication.h. For instance:
// if (floor(NSAppKitVersionNumber) <= NSAppKitVersionNumber10_6) {
//   /* On a 10.6.x or earlier system
// } else if (floor(NSAppKitVersionNumber) <= NSAppKitVersionNumber10_7) {
//   /* On a 10.7 - 10.7.x system
// } else {
//   /* Mountain Lion or later system
// }
// Special cases or situations for version checking are also discussed in the release notes as appropriate. For instance some individual headers may also declare the versions numbers for NSAppKitVersionNumber where some bug fix or functionality is available in a given update, for example:
// #define NSAppKitVersionWithSuchAndSuchBadBugFix 582.1

#define	NSAppKitVersionNumber10_6 1038
#define	NSAppKitVersionNumber10_7 1138

// Carbon event handler.
static OSStatus carbonEventHandler(EventHandlerCallRef myHandler, EventRef theEvent, void* userData)
{
	NativeUIController *controller = (NativeUIController *)userData;
	UInt32 eventKind = GetEventKind(theEvent);
	OSStatus status;
	
	if (controller->isProcessingCarbonEventHandler) {
		return eventNotHandledErr;
	}
	controller->isProcessingCarbonEventHandler = YES;
	status = eventNotHandledErr;
	switch (eventKind) {
		case kEventWindowActivated:
			[[controller window] makeKeyAndOrderFront: controller];
			status = noErr;
			break;
		case kEventWindowClose:
			[controller close];
			[[[NSApp orderedWindows] objectAtIndex: 0] makeKeyWindow];
			status = noErr;
			break;
	}
	controller->isProcessingCarbonEventHandler = NO;
	return status;
}


@implementation NativeUIController
+ (BOOL)pluginClassIsValid:(Class)pluginClass 
{
	if ([pluginClass conformsToProtocol: @protocol(AUCocoaUIBase)]) {
		if ([pluginClass instancesRespondToSelector: @selector(interfaceVersion)] &&
		    [pluginClass instancesRespondToSelector: @selector(uiViewForAudioUnit:withSize:)]) {
			return YES;
		}
	}
    return NO;
}

+ (NativeUIController *)editorForAudioUnit:(AudioUnit)unit
{
	return [self editorForAudioUnit: unit forceGeneric: FALSE delegate: nil];
}

+ (NativeUIController *)editorForAudioUnit:(AudioUnit)unit forceGeneric:(BOOL)forceGeneric delegate:(id)delegate
{
	id controller;
	
	controller = [[[NativeUIController alloc] initWithAudioUnit:unit forceGeneric:forceGeneric delegate:delegate] autorelease];
	if (controller != nil) {
		[[controller window] makeKeyAndOrderFront: nil];
	}
	return controller;
}

- (WindowRef)carbonWindowRef
{
	return carbonWindowRef;
}

- (AudioUnit)audioUnit
{
	return audioUnit;
}

- (BOOL)error:(NSString *)errString status:(OSStatus)err
{
	NSString *errorString = [NSString stringWithFormat:@"%@ failed with error code %i: %s", errString, (int)err, GetMacOSStatusCommentString(err)];
	NSLog(@"%@", errorString);
	return NO;
}

- (void)initViewComponentDescription
{
	// set up to use generic UI component
	viewComponentDescription.componentType = kAudioUnitCarbonViewComponentType;
	viewComponentDescription.componentSubType = 'gnrc';
	viewComponentDescription.componentManufacturer = 'appl';
	viewComponentDescription.componentFlags = 0;
	viewComponentDescription.componentFlagsMask = 0;		
}

- (void)detectViewComponentDescription:(BOOL)forceGeneric
{
	OSStatus err;
	UInt32 propSize;
	ComponentDescription *cds;

	if (forceGeneric) {
		return;
	}
	[self initViewComponentDescription];
	err = AudioUnitGetPropertyInfo(audioUnit, 
									kAudioUnitProperty_GetUIComponentList, 
									kAudioUnitScope_Global, 
									0, 
									&propSize, 
									NULL);
	
	if (err != noErr) {
		NSLog(@"Error setting up carbon interface, falling back to generic interface.");
		return;
	}
	
	cds = malloc(propSize);
	err = AudioUnitGetProperty(audioUnit, 
								kAudioUnitProperty_GetUIComponentList, 
								kAudioUnitScope_Global, 
								0, 
								cds, 
								&propSize);
	if (err == noErr) {
		viewComponentDescription = cds[0]; // Pick the first one
	}
	free(cds);
}

- (BOOL)hasCocoaView
{
	UInt32 dataSize = 0;
	Boolean isWritable = 0;
	
	OSStatus err = AudioUnitGetPropertyInfo(audioUnit, 
											kAudioUnitProperty_CocoaUI, 
											kAudioUnitScope_Global, 
											0, 
											&dataSize, 
											&isWritable);
	
	return (err == noErr && dataSize > 0);
}

- (NSView *)getCocoaView
{
	NSView *auView = nil;
	UInt32 dataSize = 0;
	Boolean isWritable = 0;
	
	// get AU's Cocoa view property
	OSStatus err = AudioUnitGetPropertyInfo(audioUnit, 
											kAudioUnitProperty_CocoaUI, 
											kAudioUnitScope_Global, 
											0, 
											&dataSize, 
											&isWritable);
	
	if (err != noErr) {
		[self error: @"Getting cocoa property info" status: err];
		return nil;
	}
	
	AudioUnitCocoaViewInfo *cocoaViewInfo = (AudioUnitCocoaViewInfo *) malloc(dataSize);
	//HeapBlock <AudioUnitCocoaViewInfo> cocoaViewInfo;
	//cocoaViewInfo.calloc (dataSize, 1);
	err = AudioUnitGetProperty(audioUnit, 
								kAudioUnitProperty_CocoaUI, 
								kAudioUnitScope_Global, 
								0, 
								cocoaViewInfo, 
								&dataSize);
	if (err != noErr) {
		[self error: @"Getting cocoa view info" status: err];
		return nil;
	}
	int numberOfClasses = (dataSize - sizeof(CFURLRef)) / sizeof(CFStringRef);
	// we only take the first view in this plugin.
	NSString *factoryClassName = (NSString *)(cocoaViewInfo->mCocoaAUViewClass[0]);
	NSURL *CocoaViewBundlePath	= (NSURL *)cocoaViewInfo->mCocoaAUViewBundleLocation;
	if (!(CocoaViewBundlePath && factoryClassName)) {
		[self error: @"Getting cocoa view info: bundle or factory" status: err];
		return nil;
	}
	NSBundle *viewBundle = [NSBundle bundleWithPath:[CocoaViewBundlePath path]];
	if (viewBundle == nil) {
		[self error: @"Error loading AU view's bundle" status: 0];
		return nil;
	}
	Class factoryClass = [viewBundle classNamed:factoryClassName];
	NSAssert(factoryClass != nil, @"Error getting AU view's factory class from bundle");
	// make sure 'factoryClass' implements the AUCocoaUIBase protocol
	Boolean pluginClassIsValid;
	NSAssert(pluginClassIsValid = [NativeUIController pluginClassIsValid:factoryClass],
				@"AU view's factory class does not properly implement the AUCocoaUIBase protocol");
	if (pluginClassIsValid) {
		// make a factory
		id factoryInstance = [[[factoryClass alloc] init] autorelease];
		NSAssert (factoryInstance != nil, @"Could not create an instance of the AU view factory");
		// make a view
		NSSize defaultViewSize = NSMakeSize(400, 300);
		auView = [factoryInstance uiViewForAudioUnit:audioUnit withSize:defaultViewSize];
	}
	// cleanup
	[CocoaViewBundlePath release];
	if (cocoaViewInfo != NULL) {
        int i;
        for (i = 0; i < numberOfClasses; i++) {
            CFRelease(cocoaViewInfo->mCocoaAUViewClass[i]);
        }
        //CFRelease(cocoaViewInfo->mCocoaAUViewBundleLocation);
        free(cocoaViewInfo);
        cocoaViewInfo = NULL;
    }
	return auView;
}

- (NSWindow *)createCocoaWindow
{
	if ([self hasCocoaView]) {
		NSView *view = [self getCocoaView];
		if (view) {
			NSWindow *cocoaWindow = [[[NSWindow alloc] 
							initWithContentRect: NSMakeRect(100, 
															400, 
															[view frame].size.width, 
															[view frame].size.height) 
							styleMask: NSTitledWindowMask | 
										NSClosableWindowMask | 
										NSMiniaturizableWindowMask | 
										NSResizableWindowMask 
							backing:NSBackingStoreBuffered defer:NO] 
						autorelease];
			[cocoaWindow setContentView:view];
			return cocoaWindow;
		}
	}
	return nil;
}

-(NSWindow *)createCarbonWindow
{
	OSStatus err;
	
	Component editComponent = FindNextComponent(NULL, &viewComponentDescription);
	OpenAComponent(editComponent, &auCarbonView);
	if (auCarbonView == nil) {
		[NSException raise:NSGenericException format:@"Could not open audio unit editor component"];
	}
	
	Rect bounds = { 100, 100, 100, 100 };

	err = CreateNewWindow(kDocumentWindowClass, 
							kWindowCloseBoxAttribute | 
							kWindowCollapseBoxAttribute | 
							kWindowStandardHandlerAttribute | 
							kWindowCompositingAttribute, 
							&bounds, 
							&carbonWindowRef);
	if (err != noErr) {
		[NSException raise:NSGenericException format:@"failed to create new carbon window"];
	}
	// create the edit view
	ControlRef rootControl;
	GetRootControl(carbonWindowRef, &rootControl);
	if (rootControl == nil)  {
		[NSException raise:NSGenericException format:@"could not get root control of carbon window"];
	}
	
	ControlRef viewPane;
	Float32Point loc  = { 0.0, 0.0 };
	Float32Point size = { 0.0, 0.0 } ;
	AudioUnitCarbonViewCreate(auCarbonView, 
								audioUnit, 
								carbonWindowRef, 
								rootControl, 
								&loc, 
								&size, 
								&viewPane);
	if (err != noErr) {
		[NSException raise:NSGenericException format:@"failed to create audio unit edit view"];
	} 
	
	// reposition window
	GetControlBounds(viewPane, &bounds);
	size.x = bounds.right - bounds.left;
	size.y = bounds.bottom - bounds.top;
	SizeWindow(carbonWindowRef, (short) (size.x + 0.5), (short) (size.y + 0.5),  true);
	RepositionWindow(carbonWindowRef, NULL, kWindowCenterOnMainScreen);

	// install event handler
	EventTypeSpec eventList[] = {
		{kEventClassWindow, kEventWindowActivated},
		//{kEventClassWindow, kEventWindowGetClickActivation},
		{kEventClassWindow, kEventWindowClose}
	};
	EventHandlerUPP	handlerUPP = NewEventHandlerUPP(carbonEventHandler);
	err = InstallWindowEventHandler(carbonWindowRef, 
									handlerUPP, 
									sizeof(eventList) / sizeof(eventList[0]), 
									eventList, 
									self, 
									NULL);
	if (err != noErr) {
		[self error: @"Installation of WindowClose handler" status: err];
		return nil;
	}
	// create the cocoa window for the carbon one and make it visible
	return [[[NSWindow alloc] initWithWindowRef: carbonWindowRef] autorelease];
}

- (BOOL)uglyCheck:(AudioUnit)unit
{
	Boolean versionCheck;
	Boolean unitCheck = true;
	
	versionCheck = floor(NSAppKitVersionNumber) > NSAppKitVersionNumber10_6;
	NSLog(@"versionCheck: %@", (versionCheck ? @"true":@"false"));
	if (versionCheck) {
		return versionCheck;
	}
	OSStatus err;
	AudioComponentDescription cd;
	AudioComponent component;
	component = AudioComponentInstanceGetComponent(unit);
	err = AudioComponentGetDescription(component, &cd);
	if (err != noErr) {
		return false;
	}
	// kAudioUnitSubType_Delay
	if (cd.componentType == 'aufx' && cd.componentSubType == 'dely' && cd.componentManufacturer == 'appl') {
		unitCheck = false;
	}
	// AUMultibandCompressor
	if (cd.componentType == 'aufx' && cd.componentSubType == 'mcmp' && cd.componentManufacturer == 'appl') {
		unitCheck = false;
	}
	// MiniSpillage
	if (cd.componentType == 'aumu' && cd.componentSubType == 'mspl' && cd.componentManufacturer == 'AuSp') {
		unitCheck = false;
	}
	NSLog(@"unitCheck: %@", (unitCheck ? @"true":@"false"));
	return true;
}

- (id)initWithAudioUnit:(AudioUnit)unit forceGeneric:(BOOL)forceGeneric delegate:(id)delegate
{
	NSWindow *aWindow;

	self = [self init];
	if (self == nil) {
		return nil;
	}

	audioUnit = unit;
	_delegate = delegate;
	
	// view setting
	carbonWindowRef = 0;
	[self detectViewComponentDescription: forceGeneric];
	if ([self uglyCheck: unit] && [self hasCocoaView] && ((aWindow = [self createCocoaWindow]) != nil)) {
		[self setWindow: aWindow];
		if ([aWindow delegate] == nil) {
			[aWindow setDelegate: self];
		}
	} else if ((aWindow = [self createCarbonWindow]) != nil) {
		[self setWindow: aWindow];
		if ([aWindow delegate] == nil) {
			[aWindow setDelegate: self];
		}
	} else {
		[self release];
		return nil;
	}
	[[self window] makeKeyAndOrderFront: nil];
	return self;
}

- (void)editWindowClosed
{
	// Any additional cocoa cleanup can be added here.
	[_delegate auViewWindowWillClose: self];
}

- (void)close
{
	[[self window] orderOut: self];
	[super close];
	if (carbonWindowRef) {
		DisposeWindow(carbonWindowRef);
		carbonWindowRef = 0;
	}
	[self editWindowClosed];
}

- (void)dissociateAudioUnit
{
	[self setWindow:nil];
	if (auCarbonView) {
		CloseComponent(auCarbonView);
	}
	if (carbonWindowRef) {
		DisposeWindow(carbonWindowRef);
	}
}

- (void)dealloc
{
	[self dissociateAudioUnit];
	
// 	[self setDelegate:nil];
	
	[super dealloc];
}

// - (void)setDelegate:(id)theDelegate
// {
// 	_delegate = theDelegate;
// }

@synthesize isProcessingCarbonEventHandler;
@synthesize audioUnit;
@synthesize auCarbonView;
@synthesize _delegate;
@synthesize carbonWindowRef;

@end
